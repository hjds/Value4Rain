# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Brugerinterface til VÆRDIs Regnserieværktøj.
#  
#  Programmet bruger i vid udstrækning de samme blokke som Spildevandskomiteens 
#  Regnserieværktøj, men er ikke på samme måde godkendt derfra.
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import tkinter
from tkinter import ttk
from os import startfile

# from GUItabs.RainAnalystGUI import RainAnalystGUI
# from GUItabs.StartPageGUI import StartPageGUI1
from GUItabs.ChooseTSGUI import SimpleChoiceGUI#, AdvancedChoiceGUI
from GUItabs.AddTSGUI import AddTSGUI
# from GUItabs.CreateKM2GUI import CreateKM2GUI

class Value4RainGUI():

    def __init__(self, window):
        ## Constants
        VERSNO = '1.0'#'0.9.1'
    
        ## GUI
        
        self.window = window
        self.window.geometry('800x600')
        
        self.window.title("VÆRDIs Regnserieværktøj - Version " + VERSNO)
        
        ## file menu
        menubar = tkinter.Menu(self.window)
        
        filemenu = tkinter.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Afslut", command=self.window.quit)
        menubar.add_cascade(label="Fil", menu=filemenu)
        
        filemenu = tkinter.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Åben brugermanual", command=self.readme)
        filemenu.add_command(label="Åben liste over SVK-stationer", command=self.svklist)
        filemenu.add_command(label="Åben GNU GPLv3 licens", command=self.lic)
        menubar.add_cascade(label="Dokumenter", menu=filemenu)
                
        filemenu = tkinter.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Om", command=self.about) 
        menubar.add_cascade(label="Om", menu=filemenu)
        
        self.window.config(menu=menubar)
        
        ## Create frames for the five tabs
        tab_control = ttk.Notebook(self.window)
        
        # tab0 = ttk.Frame(tab_control)
        
        tab1 = ttk.Frame(tab_control)
        
        # tab2 = ttk.Frame(tab_control)
        
        tab3 = ttk.Frame(tab_control)
        
        # tab4 = ttk.Frame(tab_control)
        
        # tab5 = ttk.Frame(tab_control)
        
        ## add the four tabs to the main frame
        # tab_control.add(tab0, text='Start')
        
        tab_control.add(tab1, text=' Udvælg regnserie ')
        
        # tab_control.add(tab2, text='Avanceret udvælgelse')
        
        tab_control.add(tab3, text=' Tilføj regnserie ')
        
        # tab_control.add(tab4, text=' Analyser regnserie ')
        
        # tab_control.add(tab5, text=' Lav ny km2-fil ')
        
        ## add content to the tabs        
        # StartPageGUI(tab0)
        
        SimpleChoiceGUI(tab1)
        
        # AdvancedChoiceGUI(tab2)
        
        AddTSGUI(tab3)
        
        # RainAnalystGUI(tab4)
        
        # CreateKM2GUI(tab5)
        
        tab_control.pack(expand=1, fill='both')
                
    def readme(self):
        startfile("Brugermanual SVKs regnserieværktøj v1.0.pdf")
        
    def svklist(self):
        startfile("data\\svkdata.txt")
        
    def lic(self):
        startfile("gpl.txt")
        
    def about(self):
        import tkinter.messagebox
        tkinter.messagebox.showinfo("Om programmet","VÆRDIs Regnserieværktøj, \nCopyright (C) 2014-2023, Danmarks Tekniske Universitet, "+
                              "\nDTU Sustain - Institut for Miljø. og Ressourceteknologi, \nHjalte Jomo Danielsen Sørup.\n\n"+
                              "Programmet kommer under en GPLv3-licens og uden nogen form for garanti; AL BRUG ER PÅ EGET ANSVAR.\n\n"+
                              "Programmet stilles frit til rådighed og kan modificeres og deles under de forhold som licensen tillader.")

if __name__ == "__main__":
    root = tkinter.Tk()
    GUI = Value4RainGUI(root)
    root.mainloop()