# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  test for the File Reader for the Engine for Spildevandskomiteens Regnværktøj
#
#  Copyright (C) 2020-2022, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

## set working directory to project directory
import os
os.chdir('C:\\Users\\hjds\\GIT-projects\\Value4rain')

## do actual testing
import unittest
from Engine.FileReader import KM2reader

class TestFileReader(unittest.TestCase):
    
    def test_KM2reader_timeseriesdata(self):
        km2reader = KM2reader("test\\testdata\\testdata.km2")
        
        ## assert time series output
        ## check that time series data has not been created
        self.assertEqual(len(km2reader.timeseriesdata), 0)
        
        ## check that it has been created now
        self.assertEqual(len(km2reader.getTimeSeriesData()), 379)
        
        ## check details
        self.assertEqual(km2reader.timeseriesdata[0], 3.333)
        self.assertEqual(km2reader.timeseriesdata[49], 0.068)
        self.assertEqual(km2reader.timeseriesdata[378], 0.222)
        self.assertEqual(km2reader.timeseriesdata[50], 0.0)
        
        self.assertEqual(km2reader.getTimeSeriesData(), km2reader.timeseriesdata)
        
    def test_KM2reader_filedata(self):
        km2reader = KM2reader("test\\testdata\\testdata.km2")
        
        ## assert file input details
        self.assertEqual(len(km2reader.filedata),28)
        self.assertEqual(km2reader.filedata[0],
                         '1 20180115 2201   5763    50  1    0.4 2    s')
        self.assertEqual(km2reader.filedata[6],
                         '1 20180116 0055   5763   205  1    2.4 2    s')
        self.assertEqual(km2reader.filedata[26],
                         '   0.222  0.222  0.222  0.222  0.222  0.222  0.222  0.222  0.222  0.222')
        
        self.assertEqual(km2reader.getOriginalData(), km2reader.filedata)
    
    def test_KM2reader_starttime(self):
        km2reader = KM2reader("test\\testdata\\testdata.km2")
        
        ## assert start time has not been created
        self.assertNotEqual(str(km2reader.global_start_time), '2018-01-15 22:01:00')
        ## check that it has been created
        self.assertEqual(str(km2reader.getStartTime()), '2018-01-15 22:01:00')
        
        self.assertEqual(km2reader.getStartTime(), km2reader.global_start_time)
        
    def test_KM2reader_getTimeSeriesLength(self):
        km2reader = KM2reader("test\\testdata\\testdata.km2")
        
        self.assertEqual(km2reader.getTimeSeriesLength(), 379./60/24/365)
        
if __name__ == '__main__':
    unittest.main()