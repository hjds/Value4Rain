# -*- coding: utf-8 -*-
"""
Created on Sun Aug  8 20:22:25 2021

@author: hjds
"""

import textwrap, os

files = os.listdir('C://Users//hjds//GIT-projects//Value4rain//_obsolete//kunRegnserier')


outputpath = 'C://Users//hjds//GIT-projects//Value4rain//_obsolete//kunRegnserier//'

for fil in files:
    ## define file names
    filenamein = outputpath + fil
    filenameout = outputpath + fil[:-4] + '_v1.1.km2'
    
    print('correcting ' + filenamein)
    
    with open(filenamein, 'r') as f:
        data = f.readlines()
        
    outdata = []
    
    for line in data:
        if line.startswith(' '):

            linedata = textwrap.wrap(line[1:],7,drop_whitespace=False)
            lineoutdata = []
            
            for i in range(0,len(linedata)-1,1):
                try:
                    if float(linedata[i]) < 0:
                        lineoutdata.append(0.001)
                    elif float(linedata[i]) > 99.999:
                        lineoutdata.append(99.999)
                    else:
                        lineoutdata.append(float(linedata[i]))
                except:
                    # lineoutdata.append(linedata[i])
                    print('>>'+linedata[i]+'<<')
                        
                
            lineoutstr = ' '
            for k in lineoutdata:
                lineoutstr = lineoutstr + "{:7.3f}".format(k)[0:7]

        else:
            lineoutstr = line[:-2]
        
        outdata.append(lineoutstr)
            
    with open(filenameout, 'w') as f:
        for o in outdata:
            f.writelines(o + '\n')
        
        