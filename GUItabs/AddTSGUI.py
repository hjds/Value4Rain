# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  GUI for Spildevandskomiteens Regnværktøj. Dette er GUI til fanen hvor man 
#  som bruger selv kan tilføje flere regnserier til værktøjet.
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import tkinter
import tkinter.filedialog
from Engine.Calculators import CalcAddTS

class AddTSGUI():
    
    def __init__(self, top):
        ## initiate variables
        self.top = top
        # self.initiateVariables()
        self.callbackcounter = 0
        
        ## GUI
        self._createFrame()
        self._createSpacing(0)
        self._createFileChooser()
        self._createWho()
        self._createHow()
        self._createSetID()       
        self._createSpacing(10)
        self._createCalculationButton("Tilføj regnserie")
        self._createOutputField()

    def _createOutputField(self):
        self.outputField = tkinter.Label(self.top, anchor='nw', justify=tkinter.LEFT)
        self.outputField.grid(columnspan=4, rowspan=2, sticky='NWSE')
        
        self.outputField['text'] = '\n>'
    
    def _createCalculationButton(self, tag):
        calcButton = tkinter.Button(self.top, text=tag, command=self.calc, font=('Helvetica', '20'))
        calcButton.grid(columnspan=4, sticky='NWSE')
        
    def _createFileChooser(self):
        header = tkinter.Label(self.inputField, text="Vælg regnserie der ønskes tilføjet:")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.regnserie = tkinter.Entry(self.inputField)
        self.regnserie.grid(row=header.grid_info()['row'], column=1, columnspan=7, sticky="WE", pady=3)
        
        findFilButton = tkinter.Button(self.inputField, text="Find fil",command=self.findFilButtonCode)
        findFilButton.grid(row=header.grid_info()['row'], column=8, columnspan=1, sticky='NWSE', padx=5, pady=2)
        
        vaelgFilLabel = tkinter.Label(self.inputField, text="Vælg filformat: ")
        vaelgFilLabel.grid(sticky='E', padx=5, pady=2)        
        
        self.filFormat = tkinter.IntVar()
        inRadio1 = tkinter.Radiobutton(self.inputField,text='.km2', variable=self.filFormat, value=1)
        inRadio1.grid(row=vaelgFilLabel.grid_info()['row'],column=1,sticky="W", pady=3)
        inRadio1.select()
        
        # inRadio2 = tkinter.Radiobutton(self.inputField,text='ASCII', variable=self.filFormat, value=2)
        # inRadio2.grid(row=vaelgFilLabel.grid_info()['row'],column=2,sticky="W", pady=3)
        
        # inRadio3 = tkinter.Radiobutton(self.inputField,text='.csv', variable=self.filFormat, value=3)
        # inRadio3.grid(row=vaelgFilLabel.grid_info()['row'],column=3, sticky="W", pady=3)
    
    def _createFrame(self):
        self.inputField = tkinter.LabelFrame(self.top, text=" Tilføj regnserie: ")
        self.inputField.grid(padx=5, pady=5, ipadx=5, ipady=5)
        
    def _createHow(self):
        header = tkinter.Label(self.inputField, text="Metode brugt til fremstilling af regnserien:")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.metode = tkinter.Entry(self.inputField)
        self.metode.grid(row=header.grid_info()['row'], column=1, columnspan=7, sticky="WE", pady=3)
        
    def _createSetID(self):
        self.ID = tkinter.IntVar()
        IDjanej = tkinter.Checkbutton(self.inputField, text="Tildel ID manuelt: ",
                              variable=self.ID,command=self.IDselect)
        IDjanej.grid(sticky='E', padx=5, pady=2)
        
        self.IDfeldt = tkinter.Entry(self.inputField)
        self.IDfeldt.insert(0,'?????')
        self.IDfeldt.grid(row=IDjanej.grid_info()['row'],column=1, columnspan=7,sticky='WE')
        self.IDfeldt.config(state='disabled')
        
    def _createSpacing(self, i):
        a = tkinter.Label(self.inputField, text="", width=34)
        b = tkinter.Label(self.inputField, text="", width=6)
        c = tkinter.Label(self.inputField, text="", width=6)
        d = tkinter.Label(self.inputField, text="", width=6)
        e = tkinter.Label(self.inputField, text="", width=6)
        f = tkinter.Label(self.inputField, text="", width=6)
        g = tkinter.Label(self.inputField, text="", width=6)
        h = tkinter.Label(self.inputField, text="", width=6)
        j = tkinter.Label(self.inputField, text="", width=25)
        a.grid(row=i,column=0)
        b.grid(row=i,column=1)
        c.grid(row=i,column=2)
        d.grid(row=i,column=3)
        e.grid(row=i,column=4)
        f.grid(row=i,column=5)
        g.grid(row=i,column=6)
        h.grid(row=i,column=7)
        j.grid(row=i,column=8)
        
    def _createWho(self):
        header = tkinter.Label(self.inputField, text="Navn på den der tilføjer regnserien:")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.hvem = tkinter.Entry(self.inputField)
        self.hvem.grid(row=header.grid_info()['row'], column=1, columnspan=7, sticky="WE", pady=3)
      
    def calc(self):
        ## call calculator with the GUI page as data package
        self.outputField['text'] = '\n>'
        self.callbackresult = '.'
        self.calculator = CalcAddTS(self,self.callback)
        self.calculator.start()
        self.top.after(2,self.checkCallBack)
        
    def callback(self, msg):
        self.outputField['text'] += msg
        self.callbackresult = '.'
    
    def checkCallBack(self):

        if self.calculator.is_alive():
            self.top.after(1,self.checkCallBack)
            if self.callbackresult == '.':
                self.callbackcounter += 1
                if self.callbackcounter > 200:
                    self.outputField['text'] += self.callbackresult
                    self.callbackcounter = 0
            else:
                self.outputField['text'] += self.callbackresult
                self.callbackresult = '.'
                self.callbackcounter = 0
        else:
            self.outputField['text'] += self.callbackresult
        
    def IDselect(self):
        if self.ID.get():
            self.IDfeldt.config(state='normal')
        else:
            self.IDfeldt.config(state='disabled')
            
    def findFilButtonCode(self):
        self.regnserie.delete(0, 'end')
        self.regnserie.insert(0,tkinter.filedialog.askopenfilename())