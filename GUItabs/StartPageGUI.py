# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  GUI for Spildevandskomiteens Regnværktøj
#
#  Copyright (C) 2020, Department of Environmental Engineering
#  Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import tkinter

class StartPageGUI():
    
    def __init__(self, top):
        
        self.lbl0 = tkinter.Label(top, text= 'Starttekst, forklaring af hvordan man bruger programmet osv.')
        
        self.lbl0.grid(column=0, row=0)