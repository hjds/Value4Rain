# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  GUI for Spildevandskomiteens Regnværktøj. Dette er GUI til fanen hvor man 
#  kan vælge en tidsserie der passer til en bestemt lokation.
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import tkinter
from Engine.Calculators import CalcSimple, CalcAdvanced

class SimpleChoiceGUI():
    
    def __init__(self, top):
        ## initiate variables
        self.top = top
        self.initiateVariables()
        self.callbackcounter = 0
        
        ## GUI
        self._createGUI()
    
    def _createCalculationButton(self, tag):
        calcButton = tkinter.Button(self.top, text=tag, command=self.calc, font=('Helvetica', '20'))
        calcButton.grid(columnspan=4, sticky='NWSE')
        
    def _createClimate(self):
        ## Klima
        header = tkinter.Label(self.inputField, text="Klima [scenarie]:")
        header.grid(sticky='E', padx=5, pady=2)

        klimascenarie = tkinter.OptionMenu(self.inputField,self.klima, *self.klimas)
        klimascenarie.grid(row=header.grid_info()['row'], column=1, columnspan=3, sticky="NSWE", pady=3)
        
    def _createClimateFactor(self):
        header = tkinter.Label(self.inputField, text="Klimafaktorer:")
        header.grid(sticky='E', padx=5, pady=2)
        
        header2aar = tkinter.Label(self.inputField, text="T=2 år:")
        header2aar.grid(row=header.grid_info()['row'], column=1, padx=5, pady=2)
        
        header10aar = tkinter.Label(self.inputField, text="T=10 år:")
        header10aar.grid(row=header.grid_info()['row'], column=2, padx=5, pady=2)
        
        header100aar = tkinter.Label(self.inputField, text="T=100 år:")
        header100aar.grid(row=header.grid_info()['row'], column=3, padx=5, pady=2)
        
        header1time = tkinter.Label(self.inputField, text="1 time:")
        header1time.grid(sticky='E', padx=5, pady=2)
        
        self.klima2aar1time = tkinter.Entry(self.inputField, justify='center')
        self.klima2aar1time.grid(row=header1time.grid_info()['row'],column=1,sticky="WE", pady=3)
        
        self.klima10aar1time = tkinter.Entry(self.inputField, justify='center')
        self.klima10aar1time.grid(row=header1time.grid_info()['row'],column=2,sticky="WE", pady=3)
        
        self.klima100aar1time = tkinter.Entry(self.inputField, justify='center')
        self.klima100aar1time.grid(row=header1time.grid_info()['row'],column=3,sticky="WE", pady=3)
        
        header24timer = tkinter.Label(self.inputField, text="24 timer:")
        header24timer.grid(sticky='E', padx=5, pady=2)
        
        self.klima2aar24timer = tkinter.Entry(self.inputField, justify='center')
        self.klima2aar24timer.grid(row=header24timer.grid_info()['row'],column=1,sticky="WE", pady=3)
        
        self.klima10aar24timer = tkinter.Entry(self.inputField, justify='center')
        self.klima10aar24timer.grid(row=header24timer.grid_info()['row'],column=2,sticky="WE", pady=3)
        
        self.klima100aar24timer = tkinter.Entry(self.inputField, justify='center')
        self.klima100aar24timer.grid(row=header24timer.grid_info()['row'],column=3,sticky="WE", pady=3)
        
        headerVinter = tkinter.Label(self.inputField, text="Vinter:")
        headerVinter.grid(sticky='E', padx=5, pady=2)
        
        self.klimaVinter = tkinter.Entry(self.inputField, justify='center')
        self.klimaVinter.grid(row=headerVinter.grid_info()['row'],column=1,sticky="WE", pady=3)
        
        headerForaar = tkinter.Label(self.inputField, text="Forår:")
        headerForaar.grid(row=headerVinter.grid_info()['row'],column=2,sticky='E', padx=5, pady=2)
        
        self.klimaForaar = tkinter.Entry(self.inputField, justify='center')
        self.klimaForaar.grid(row=headerVinter.grid_info()['row'],column=3,sticky="WE", pady=3)
        
        headerSommer = tkinter.Label(self.inputField, text="Sommer:")
        headerSommer.grid(sticky='E', padx=5, pady=2)
        
        self.klimaSommer = tkinter.Entry(self.inputField, justify='center')
        self.klimaSommer.grid(row=headerSommer.grid_info()['row'],column=1,sticky="WE", pady=3)
        
        headerEfteraar = tkinter.Label(self.inputField, text="Efterår:")
        headerEfteraar.grid(row=headerSommer.grid_info()['row'],column=2,sticky='E', padx=5, pady=2)
        
        self.klimaEfteraar = tkinter.Entry(self.inputField, justify='center')
        self.klimaEfteraar.grid(row=headerSommer.grid_info()['row'],column=3,sticky="WE", pady=3)
        
        ## sæt klimavariable til initialværdier
        self.klima2aar1time.insert(0,"1.2")
        self.klima10aar1time.insert(0,"1.3")
        self.klima100aar1time.insert(0,"1.4")
        self.klima2aar24timer.insert(0,"1.2")
        self.klima10aar24timer.insert(0,"1.3")
        self.klima100aar24timer.insert(0,"1.4")
        self.klimaVinter.insert(0,"1.2")
        self.klimaForaar.insert(0,"1.1")
        self.klimaSommer.insert(0,"1.1")
        self.klimaEfteraar.insert(0,"1.2")
    
    def _createDurations(self):
        ## set duration and return period
        self.durheader = tkinter.Label(self.inputField, text="Spænd af varigheder [minutter], fra: ")
        self.durheader.grid(sticky='E', padx=5, pady=2)
        
        varFra = tkinter.OptionMenu(self.inputField,self.varighederFRA,
                                             *self.varigheder)
        varFra.grid(row=self.durheader.grid_info()['row'],column=1,sticky="NSWE", pady=3)
        
        headerTil = tkinter.Label(self.inputField, text="til: ")
        headerTil.grid(row=self.durheader.grid_info()['row'],column=2, sticky='E', padx=5, pady=2)
        
        varTil = tkinter.OptionMenu(self.inputField,self.varighederTIL,
                                             *self.varigheder)#, width=20)
        varTil.grid(row=self.durheader.grid_info()['row'],column=3,sticky="NSWE", pady=3)        
    
    def _createFindLocationFromSVK(self):
        ## find location based on svk station number
        self.SVKno = tkinter.Entry(self.inputField, justify='center')
        self.SVKno.grid(column=1,sticky="WE", pady=3)
        
        findLocationBtn = tkinter.Button(self.inputField, text="Find placering fra SVK stationsnummer",command=self.findLocation)
        findLocationBtn.grid(row=self.SVKno.grid_info()['row'],column=2, columnspan=2, sticky='NWSE', padx=5, pady=2)
    
    def _createFrame(self, tag):
        ## create frame
        self.inputField = tkinter.LabelFrame(self.top, text=tag)
        self.inputField.grid(sticky='NWSE', padx=5, pady=5, ipadx=5, ipady=5)

    def _createGUI(self):
        self._createFrame(' Udvælg regnserie: ')
        self._createSpacing(0)
        self._createLocation()
        self._createMetrik()
        self._createDurations()
        self._createReturnperiod()
        self._createClimate()
        self._createSpacing(12)
        self._createCalculationButton("Find regnsserie")
        self._createOutputField()

    def _createMetrik(self):
        header = tkinter.Label(self.inputField, text="Vælg optimeringsvariable:")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.metrik = tkinter.IntVar()
        inRadio1 = tkinter.Radiobutton(self.inputField,text='Intensiteter', variable=self.metrik, value=1, command=lambda: self.setIntensity(1, header.grid_info()['row']))
        inRadio1.grid(row=header.grid_info()['row'],column=1, columnspan=1, sticky="NWSE")
        inRadio1.select()
        
        inRadio2 = tkinter.Radiobutton(self.inputField,text='Afløbstal', variable=self.metrik, value=2, command=lambda: self.setAValues(2, header.grid_info()['row']))
        inRadio2.grid(row=header.grid_info()['row'],column=2, columnspan=1, sticky="NWSE")

    def _createOutputField(self):
        self.outputField = tkinter.Label(self.top, anchor='nw', justify=tkinter.LEFT)
        self.outputField.grid(columnspan=4, rowspan=2, sticky='NWSE')
        
        self.outputField['text'] = '\n>'

    def _createLocation(self):
        ## set lokation
        header = tkinter.Label(self.inputField, text="Vælg kommune:")
        header.grid(sticky='E', padx=5, pady=2)

        municipal = tkinter.OptionMenu(self.inputField,self.kommune,*self.kommuner)
        municipal.grid(row=header.grid_info()['row'],column=1, columnspan=3, sticky="WE", pady=3)
        
    def _createNorthingEasting(self):
        header = tkinter.Label(self.inputField, text="UTM-32: Northing: ")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.northing = tkinter.Entry(self.inputField, justify='center')
        self.northing.grid(row=header.grid_info()['row'],column=1,sticky="WE", pady=3)
        
        headerEasting = tkinter.Label(self.inputField, text="Easting: ")
        headerEasting.grid(row=header.grid_info()['row'],column=2, sticky='E', padx=5, pady=2)
        
        self.easting = tkinter.Entry(self.inputField, justify='center')
        self.easting.grid(row=header.grid_info()['row'],column=3,sticky="WE", pady=3) 
        
        self.setNorthingEasting()

    def _createReturnperiod(self):
        header = tkinter.Label(self.inputField, text="Gentagelsesperioder [år], fra: ")
        header.grid(sticky='E', padx=5, pady=2)
        
        returnPeriodFrom = tkinter.OptionMenu(self.inputField,self.gentagelsesperioderFRA,
                                             *self.gentagelsesperioder)#,width=20)
        returnPeriodFrom.grid(row=header.grid_info()['row'],column=1, sticky="NSWE", pady=3)
        
        headerTil = tkinter.Label(self.inputField, text="til: ")
        headerTil.grid(row=header.grid_info()['row'],column=2, sticky='E', padx=5, pady=2)
        
        returnPeriodTo = tkinter.OptionMenu(self.inputField,self.gentagelsesperioderTIL,
                                             *self.gentagelsesperioder)#,width=20)
        returnPeriodTo.grid(row=header.grid_info()['row'],column=3,sticky="NSWE", pady=3)
        
    def _createSeriesLength(self):
        header = tkinter.Label(self.inputField, text="Hvor mange års regndata ønskes: ")
        header.grid(sticky='E', padx=5, pady=2)
        
        self.regnserielaengde = tkinter.Entry(self.inputField, justify='center')
        self.regnserielaengde.grid(row=header.grid_info()['row'],column=1,sticky="WE", pady=3)
        self.regnserielaengde.insert(0,"20")
    
    def _createSpacing(self, i):
        a = tkinter.Label(self.inputField, text="", width=32)
        b = tkinter.Label(self.inputField, text="", width=25)
        c = tkinter.Label(self.inputField, text="", width=25)
        d = tkinter.Label(self.inputField, text="", width=25)
        a.grid(row=i,column=0)
        b.grid(row=i,column=1)
        c.grid(row=i,column=2)
        d.grid(row=i,column=3)
    
    def calc(self):
        ## call calculator with the GUI page as data package
        self.outputField['text'] = '\n>'
        self.callbackresult = '.'
        self.calculator = CalcSimple(self,self.callback)
        self.calculator.start()
        self.top.after(2,self.checkCallBack)
        
    def callback(self, msg):
        self.outputField['text'] += msg
        self.callbackresult = '.'
    
    def checkCallBack(self):

        if self.calculator.is_alive():
            self.top.after(1,self.checkCallBack)
            if self.callbackresult == '.':
                self.callbackcounter += 1
                if self.callbackcounter > 200:
                    self.outputField['text'] += self.callbackresult
                    self.callbackcounter = 0
            else:
                self.outputField['text'] += self.callbackresult
                self.callbackresult = '.'
                self.callbackcounter = 0
        else:
            self.outputField['text'] += self.callbackresult
        
    def findLocation(self):
        # import DTURainAnalystEngine as engine
        # ## find northing and easting
        # a,b = engine.get_svk_data_northing_easting(self.inFileTxt.get())
        # dummy for a and b inserted instead of call to engine
        a = '12345'
        b = '54321'
        if len(a) > 1:
            self.northing.delete(0, 'end')
            self.easting.delete(0, 'end')
            self.northing.insert(0,a)
            self.easting.insert(0,b)
        else:
            tkinter.messagebox.showinfo("Fejl","Det var ikke muligt at finde en placering ud fra filnavnet.")
            
    def initiateVariables(self):
        # ## formål
        # with open('data\\formål.txt','r') as f:
        #     self.purposes = f.readlines()
        # self.purpose = tkinter.StringVar(self.top)
        # self.purpose.set(self.purposes[0])
        
        ## placering
        with open('data\\kommuner.txt','r') as f:
            self.kommuner = f.readlines()
        self.kommune = tkinter.StringVar(self.top)
        self.kommune.set(self.kommuner[63])

        ## klima
        with open('data\\klima.txt','r') as f:
            self.klimas = f.readlines()
        self.klima = tkinter.StringVar(self.top)
        self.klima.set(self.klimas[0])
        
        ## varigheder
        with open('data\\varigheder.txt','r') as f:
            self.varigheder = f.readlines()
        self.varigheder = self.varigheder[2:]
        self.varighederFRA = tkinter.StringVar(self.top)
        self.varighederFRA.set(self.varigheder[0])
        self.varighederTIL = tkinter.StringVar(self.top)
        self.varighederTIL.set(self.varigheder[-1])
    
        ## gentagelsesperioder
        with open('data\\gentagelsesperioder.txt','r') as f:
            self.gentagelsesperioder = f.readlines()
        self.gentagelsesperioderFRA = tkinter.StringVar(self.top)
        self.gentagelsesperioderFRA.set(self.gentagelsesperioder[0])
        self.gentagelsesperioderTIL = tkinter.StringVar(self.top)
        self.gentagelsesperioderTIL.set(self.gentagelsesperioder[-1])
        self.gentagelsesperioderFULL = self.gentagelsesperioder
        self.gentagelsesperioderAVALUES = self.gentagelsesperioder[1:-3]
        
        ## afløbstal
        with open('data\\afløbstal.txt','r') as f:
            self.avalues = f.readlines()
        self.avaluesFRA = tkinter.StringVar(self.top)
        self.avaluesFRA.set(self.avalues[0])
        self.avaluesTIL = tkinter.StringVar(self.top)
        self.avaluesTIL.set(self.avalues[-1])
    
    def setAValues(self, choice, rownumber):
        header = tkinter.Label(self.inputField, text="             Spænd af afløbstal ["+chr(956)+"m/s], fra: ")
        header.grid(row=rownumber+1,column=0,sticky='E', padx=5, pady=2)
        
        varFra = tkinter.OptionMenu(self.inputField,self.avaluesFRA,
                                             *self.avalues)
        varFra.grid(row=rownumber+1,column=1,sticky="NSWE", pady=3)
        
        headerTil = tkinter.Label(self.inputField, text="til: ")
        headerTil.grid(row=rownumber+1,column=2, sticky='E', padx=5, pady=2)
        
        varTil = tkinter.OptionMenu(self.inputField,self.avaluesTIL,
                                             *self.avalues)#, width=20)
        varTil.grid(row=rownumber+1,column=3,sticky="NSWE", pady=3)

        self.gentagelsesperioder = self.gentagelsesperioderAVALUES
        self.gentagelsesperioderFRA.set(self.gentagelsesperioder[0])
        self.gentagelsesperioderTIL.set(self.gentagelsesperioder[-1])

    
    def setIntensity(self, choice, rownumber):
        header = tkinter.Label(self.inputField, text="   Spænd af varigheder [minutter], fra: ")
        header.grid(row=rownumber+1,column=0,sticky='E', padx=5, pady=2)
        
        varFra = tkinter.OptionMenu(self.inputField,self.varighederFRA,
                                             *self.varigheder)
        varFra.grid(row=rownumber+1,column=1,sticky="NSWE", pady=3)
        
        headerTil = tkinter.Label(self.inputField, text="til: ")
        headerTil.grid(row=rownumber+1,column=2, sticky='E', padx=5, pady=2)
        
        varTil = tkinter.OptionMenu(self.inputField,self.varighederTIL,
                                             *self.varigheder)#, width=20)
        varTil.grid(row=rownumber+1,column=3,sticky="NSWE", pady=3)
        
        self.gentagelsesperioder = self.gentagelsesperioderFULL
        self.gentagelsesperioderFRA.set(self.gentagelsesperioder[0])
        self.gentagelsesperioderTIL.set(self.gentagelsesperioder[-1])
    
    def setNorthingEasting(self):
        ## make algorithm that relates kommune to northing/easting
        self.northing.insert(0,"0")
        self.easting.insert(0,"0")
        
class AdvancedChoiceGUI(SimpleChoiceGUI):
    
    ## __init__ call inhertied from SimpelChoiceGUI!
    
    def _createGUI(self):
        self._createFrame(' Avanceret udvælgelse af regnserier: ')
        self._createSpacing(0)
        self._createNorthingEasting()
        self._createFindLocationFromSVK()
        self._createDurations()
        self._createReturnperiod()
        self._createSeriesLength()
        self._createClimateFactor()
        self._createSpacing(12)
        self._createCalculationButton("Find regnsserie(r)")    
        
    def calc(self):
        msg = "Nothing: " + self.northing.get().strip() + "\n"
        msg = msg + "Easting: " + self.easting.get().strip() + "\n"
        msg = msg + "Vargheder: " + self.varighederFRA.get().strip() + ", " + self.varighederTIL.get().strip() + "\n"
        msg = msg + "Gentagelsesperioder: " + self.gentagelsesperioderFRA.get().strip() + ", " + self.gentagelsesperioderTIL.get().strip() + "\n"
        msg = msg + "Klimafaktorer 1 time: " + self.klima2aar1time.get().strip() + ", " + self.klima10aar1time.get().strip() + ", " + self.klima100aar1time.get().strip() + "\n"
        msg = msg + "Klimafaktorer 24 timer: " + self.klima2aar24timer.get().strip() + ", " + self.klima10aar24timer.get().strip() + ", " + self.klima100aar24timer.get().strip() + "\n"
        msg = msg + "Klimafaktorer sæson: " + self.klimaVinter.get().strip() + ", " + self.klimaForaar.get().strip() + ", " + self.klimaSommer.get().strip() + ", " + self.klimaEfteraar.get().strip() + "\n"
        self.calculator = CalcAdvanced(msg,self.callback)
        self.calculator.start()
        self.top.after(2,self.checkCallBack)
        
    

        
 

        