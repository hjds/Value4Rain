# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  GUI for Spildevandskomiteens Regnværktøj, baseret på koden til 
#  DTU Rain Analyst fra 2014. Dette er GUI'en for den tab der bruges til 
#  analyse af regnseirer
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import tkinter
import tkinter.filedialog
# import tkinter.filedialog
from Engine.Calculators import CalcRainAnalyst

class RainAnalystGUI:
    
    def __init__(self, top):    
        ## variables
        self.callbackresult = []
        self.callbackcounter = 0
        self.calculators = []
        
        ## input part
        self.top = top
        self._createInput()
        self._createOutput()
        self._createCalculationButton()
        self._createOutputField()

    def _createOutputField(self):
        self.outputField = tkinter.Label(self.top, anchor='nw', justify=tkinter.LEFT)
        self.outputField.grid(columnspan=4, rowspan=2, sticky='NWSE')
        
        self.outputField['text'] = '\n>'        

    def _createCalculationButton(self):
        calcButton = tkinter.Button(self.top, text ="Analyser regnserie", command=self.calc, font=('Helvetica', '20'))
        calcButton.grid(column=0,row=9, columnspan=7, sticky='NWSE')

    
    def _createInput(self):
        self.inputField = tkinter.LabelFrame(self.top, text=" Detaljer på inputfil: ")
        self.inputField.grid(row=0, columnspan=7, rowspan=4, sticky='NWSE', \
                         padx=5, pady=5, ipadx=5, ipady=5)
            
        self._createSpacingInput(0)
        
        header = tkinter.Label(self.inputField, text="Vælg fil:")
        header.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        
        self.regnserie = tkinter.Entry(self.inputField,width=30)
        self.regnserie.grid(row=0, column=1, columnspan=5, sticky="WE", pady=3)
        
        header1 = tkinter.Label(self.inputField, text="Vælg filformat: ")
        header1.grid(row=1, column=0, sticky='E', padx=5, pady=2)
        
        self.fileFormat = tkinter.IntVar()
        inRadio1 = tkinter.Radiobutton(self.inputField,text='.km2', variable=self.fileFormat, value=1)
        inRadio1.grid(row=1, column=1, columnspan=1, sticky="WE", pady=3)
        inRadio1.select()
        
        # inRadio2 = tkinter.Radiobutton(self.inputField,text='ASCII', variable=self.fileFormat, value=2,state="disabled")
        # inRadio2.grid(row=1, column=2, columnspan=1, sticky="WE", pady=3)
        
        # inRadio3 = tkinter.Radiobutton(self.inputField,text='.csv', variable=self.fileFormat, value=3,state='disabled')
        # inRadio3.grid(row=1, column=3, columnspan=1, sticky="WE", pady=3)
        
        header2 = tkinter.Label(self.inputField, text="UTM-32: Northing: ")
        header2.grid(row=2, column=0, sticky='E', padx=5, pady=2)
        
        self.northing = tkinter.Entry(self.inputField,width=20)
        self.northing.grid(row=2, column=1, columnspan=2, sticky="WE", pady=3)
        
        header3 = tkinter.Label(self.inputField, text="Easting: ")
        header3.grid(row=2, column=3, sticky='E', padx=5, pady=2)
        
        self.easting = tkinter.Entry(self.inputField,width=20)
        self.easting.grid(row=2, column=4, columnspan=2, sticky="WE", pady=3)
        
        findLocationBtn = tkinter.Button(self.inputField, text="Find placering",command=self.findLocation)
        findLocationBtn.grid(row=2, column=6, sticky='NWSE', padx=5, pady=2)
        
        inFileBtn = tkinter.Button(self.inputField, text="Vælg fil",command=self.findInputFile)
        inFileBtn.grid(row=0, column=6, sticky='NWSE', padx=5, pady=2)
        
        self.regnserieLength = tkinter.IntVar()
        inType1 = tkinter.Checkbutton(self.inputField, text="Sæt længden på regnserien manuelt [år]:",
                              variable=self.regnserieLength,command=self.setRegnserieLength)
        inType1.grid(row=3, column=0, columnspan=4,  sticky='E', padx=5, pady=2)
        
        self.regnserieLEngthFeldt = tkinter.Entry(self.inputField)
        self.regnserieLEngthFeldt.insert(0,'12.5')
        self.regnserieLEngthFeldt.grid(row=3, column=4,columnspan=2, sticky='WE')
        self.regnserieLEngthFeldt.config(state='disabled')
        
        self._createSpacingInput(10)
        
    def _createOutput(self):
        
        self.outputField = tkinter.LabelFrame(self.top, text=" Detaljer på outputfil: ")
        self.outputField.grid(row=4, column=0, columnspan=7, rowspan=4, sticky='NWSE', \
                         padx=5, pady=5, ipadx=5, ipady=5)
        
        self._createSpacingOutput(0)
            
        header = tkinter.Label(self.outputField, text="  Vælg sti: ")
        header.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        
        self.outputSti = tkinter.Entry(self.outputField)
        self.outputSti.grid(row=0, column=1, columnspan=7, sticky="WE", pady=3)
        
        outFileBtn = tkinter.Button(self.outputField, text="Vælg sti",command=self.setOutputSti)
        outFileBtn.grid(row=0, column=8, sticky='WE', padx=5, pady=2)
        
        outputHeader = tkinter.Label(self.outputField, text="Output:")
        outputHeader.grid(row=1, column=0, sticky='E', padx=5, pady=2)
        
        outputHeader2 = tkinter.Label(self.outputField, text="Outputendelser\n{inputfilnavn} + :")
        outputHeader2.grid(row=1, column=4, rowspan=2, sticky='NE', padx=5, pady=2)
        
        self.eventList = tkinter.IntVar()
        outType1 = tkinter.Checkbutton(self.outputField, text="Hændelsesliste", variable=self.eventList,command=self.setEvents)
        outType1.grid(row=1, column=1, columnspan=3, pady=2, sticky='W')
        
        self.eventListeEndelse = tkinter.Entry(self.outputField)
        self.eventListeEndelse.grid(row=1, column=5, columnspan=3, pady=2, sticky='NWSE')
        self.eventListeEndelse.insert(0,'_Events.txt')
        self.eventListeEndelse.config(state='disabled')
        
        self.rankedList = tkinter.IntVar()
        outType2 = tkinter.Checkbutton(self.outputField, text="Rangerede hændelser", variable=self.rankedList,command=self.setRanked)
        outType2.grid(row=2, column=1, columnspan=3, pady=2, sticky='W')
        
        self.rankedListEndelse = tkinter.Entry(self.outputField)
        self.rankedListEndelse.grid(row=2, column=5, columnspan=3, pady=2, sticky='NWSE')
        self.rankedListEndelse.insert(0,'_Ranked.txt')
        self.rankedListEndelse.config(state='disabled')
        
        self.ufValues = tkinter.IntVar()
        outType3 = tkinter.Checkbutton(self.outputField, text="U- og f-værdier", variable=self.ufValues,
                               command=self.setUfValues)#, state=DISABLED)
        outType3.grid(row=3, column=1, columnspan=3, pady=2, sticky='W')
        
        self.uValuesEndelse = tkinter.Entry(self.outputField)
        self.uValuesEndelse.grid(row=3, column=5, columnspan=3, pady=2, sticky='NWSE')
        self.uValuesEndelse.insert(0,'_Ufvalues.txt')
        self.uValuesEndelse.config(state='disabled')
        
        self.fValuesEndelse = tkinter.Entry(self.outputField)
        self.fValuesEndelse.grid(row=4, column=5, columnspan=3, pady=2, sticky='NWSE')
        self.fValuesEndelse.insert(0,'_parameters.txt')
        self.fValuesEndelse.config(state='disabled')
        
        self._createSpacingOutput(10)
        
    def _createSpacingInput(self, i):
        a = tkinter.Label(self.inputField, text="", width=35)
        b = tkinter.Label(self.inputField, text="", width=9)
        c = tkinter.Label(self.inputField, text="", width=9)
        d = tkinter.Label(self.inputField, text="", width=9)
        e = tkinter.Label(self.inputField, text="", width=9)
        f = tkinter.Label(self.inputField, text="", width=9)
        j = tkinter.Label(self.inputField, text="", width=25)
        a.grid(row=i,column=0)
        b.grid(row=i,column=1)
        c.grid(row=i,column=2)
        d.grid(row=i,column=3)
        e.grid(row=i,column=4)
        f.grid(row=i,column=5)
        j.grid(row=i,column=6)

    def _createSpacingOutput(self, i):
        a = tkinter.Label(self.outputField, text="", width=35)
        b = tkinter.Label(self.outputField, text="", width=4)
        c = tkinter.Label(self.outputField, text="", width=4)
        d = tkinter.Label(self.outputField, text="", width=4)
        e = tkinter.Label(self.outputField, text="", width=6)
        f = tkinter.Label(self.outputField, text="", width=4)
        g = tkinter.Label(self.outputField, text="", width=4)
        h = tkinter.Label(self.outputField, text="", width=4)        
        j = tkinter.Label(self.outputField, text="", width=20)
        a.grid(row=i,column=0)
        b.grid(row=i,column=1)
        c.grid(row=i,column=2)
        d.grid(row=i,column=3)
        e.grid(row=i,column=4)
        f.grid(row=i,column=5)
        g.grid(row=i,column=6)
        h.grid(row=i,column=7)
        j.grid(row=i,column=8)
        
    def calc(self):
        ## call calculator with the GUI page as data package
        self.outputField['text'] = '\n>'
        self.callbackresult = '.'
        self.calculator = CalcRainAnalyst(self,self.callback)
        self.calculator.start()
        self.top.after(2,self.checkCallBack)
        
    def callback(self, msg):
        self.outputField['text'] += msg
        self.callbackresult = '.'
    
    def checkCallBack(self):

        if self.calculator.is_alive():
            self.top.after(1,self.checkCallBack)
            if self.callbackresult == '.':
                self.callbackcounter += 1
                if self.callbackcounter > 200:
                    self.outputField['text'] += self.callbackresult
                    self.callbackcounter = 0
            else:
                self.outputField['text'] += self.callbackresult
                self.callbackresult = '.'
                self.callbackcounter = 0
        else:
            self.outputField['text'] += self.callbackresult
    
    def findLocation(self):
        ## find station number from first line of input file
        with open(self.regnserie.get(), 'r') as f:
            stationno = f.readline().split()[3]
        
        ## open file with SVK gauge locations
        with open('data//svkdata.txt', 'r') as f:
            svklocs = f.readlines()
        
        n='0'
        e='0'
        
        for svk in svklocs:
            if stationno == svk.split()[0] or stationno == svk.split()[1]:
                n = svk.split()[2]
                e = svk.split()[3][:-1]
                break
        
        if len(n) > 1:
            self.northing.delete(0, 'end')
            self.easting.delete(0, 'end')
            self.northing.insert(0,n)
            self.easting.insert(0,e)
        else:
            tkinter.messagebox.showinfo("Fejl","Det var ikke muligt at finde en placering ud fra filen.")
            
    def findInputFile(self):
        self.regnserie.delete(0, 'end')
        self.regnserie.insert(0,tkinter.filedialog.askopenfilename())
            
    def setRegnserieLength(self):
        if self.regnserieLength.get():
            self.regnserieLEngthFeldt.config(state='normal')
            self.regnserieLEngthFeldt.delete(0, 'end')
        else:
            self.regnserieLEngthFeldt.config(state='disabled')
            
    def setOutputSti(self):
        self.outputSti.delete(0, 'end')
        self.outputSti.insert(0,tkinter.filedialog.askdirectory())
        
    def setEvents(self):
        if self.eventList.get():
            self.eventListeEndelse.config(state='normal')
        else:
            self.eventListeEndelse.config(state='disabled')
            
    def setRanked(self):
        if self.rankedList.get():
            self.rankedListEndelse.config(state='normal')
        else:
            self.rankedListEndelse.config(state='disabled')
            
    def setUfValues(self):
        if self.ufValues.get():
            self.uValuesEndelse.config(state='normal')
            self.fValuesEndelse.config(state='normal')
        else:
            self.uValuesEndelse.config(state='disabled')
            self.fValuesEndelse.config(state='disabled')


        