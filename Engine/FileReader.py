# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  File Reader for the Engine for Spildevandskomiteens Regnværktøj
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import os
import datetime
import math
import numpy as np
# from scipy.ndimage.filters import uniform_filter1d
from Engine.RegionalModel import RegionalModel
import pandas as pd

class KM2reader():
    def __init__(self, filelocation):
        
        with open(filelocation, 'r') as f:
            self.filedata = f.readlines()
        
        for i in range(len(self.filedata)):
            self.filedata[i] = self.filedata[i].rstrip("\n")
        
        self.timeseriesdata = []
        self.timestep = datetime.timedelta(0, 60) ## one minute data
        self.global_start_time = 0
        self.lengthInYears = 0
        self.eventmaxdata = []
        
        
    def calculate_MAP(self):
        self.getTimeSeriesLength()
        
        self.MAP = float(sum(self.timeseriesdata) * 60 / 1000) / self.lengthInYears

    def calculate_zT_from_observations(self):

        with open('data//gentagelsesperioder.txt', 'r') as f:
            data = f.readlines()
            self.return_periods = [float(x) for x in data]
            
        self.regionalmodel = RegionalModel()
        self.z0values = self.regionalmodel.z0values[2:]
        
        self.create_event_max_data()
        self.getTimeSeriesLength()
        
        self.zTs = []
        self.n10s = []
        
        for j in range(len(self.aggregation_periods)):
            z0 = self.z0values[j] + 0.000001 ## To secure that rounding errors does not cause extra extremes to be included
            data2 = np.array(self.eventmaxdata[j])
            data3 = sorted(data2[data2 > z0] - z0, reverse=True)
            data4 = sorted(data2, reverse=True)
    
            n = len(data3)
            n2 = float(n)
            l = n2/(float(self.lengthInYears))
            m = np.mean(data3)
            b = 0
            for i in range(1,n):
                tmp = (n2-i)/(n2*(n2-1))*(float(data3[i-1])) 
                b = b + tmp
            a = m*(1/((2*b-m)/m)-1)
            k = m/(2*b-m)-2
            
            zTtemp = []    
            for T in self.return_periods:
                zT = z0 + a/k * (1- math.pow(1/(l*T),k)) ## ændret efter input fra Søren / ændret igen efter input fra Henrik
                zTtemp.append(zT)
    
            self.zTs.append(zTtemp)
            try:
                self.n10s.append(data4[int(self.lengthInYears*10)])
            except:
                self.n10s.append(data4[-1])
    
    def create_event_max_data(self):
        if len(self.eventmaxdata) < 1:
            with open('data//varigheder.txt', 'r') as f:
                data = f.readlines()
                self.aggregation_periods = [int(x) for x in data]
            ## only take periods above 5 minutes
            self.aggregation_periods = self.aggregation_periods[2:]
                
            for a in self.aggregation_periods:
                self.eventmaxdata.append(self.getMeanMaxIntensities(a))

    def create_alternative_km2_file(self, dryperiodh, filenameout):
        
        ## initialisering
        i = 0
        eventdata = []
        output = []
        dryperiodmin = dryperiodh*60
        
        # get the relevant meta data for the first event
        meta = self.filedata[i].split()
        start_y = meta[1][0:4]
        start_mo = meta[1][4:6]
        start_d = meta[1][6:8]
        start_h = meta[2][0:2]
        start_mi = meta[2][2:4]
        eventlen = int(meta[4])
        nlines = int(math.ceil(float(meta[4])/10))
        output.append(self.filedata[i][:22])
        
        ## set the current starttime of the event
        f_start = datetime.datetime(int(start_y), int(start_mo),
                                    int(start_d), int(start_h), int(start_mi))
        f_end = f_start + datetime.timedelta(minutes=eventlen)

        # load the full event
        for j in range(0, nlines):
            ldata = self.filedata[i+j+1].split()
            for k in range(len(ldata)):
                eventdata.append(float(ldata[k]))
        
        i = i + nlines + 1 
        
        ## look at the next events
        while i < len(self.filedata):
            # get the relevant meta data for the first event
            meta = self.filedata[i].split()
            start_y = meta[1][0:4]
            start_mo = meta[1][4:6]
            start_d = meta[1][6:8]
            start_h = meta[2][0:2]
            start_mi = meta[2][2:4]
            eventlen = int(meta[4])
            nlines = int(math.ceil(float(meta[4])/10))
            
            f_start2 = datetime.datetime(int(start_y), int(start_mo),
                                        int(start_d), int(start_h), int(start_mi))
            
            diff = f_start2 - f_end
            if diff.total_seconds()/60 > dryperiodmin:
            ## if there is too long time to the next event
                output[-1] = (output[-1] + ' ' + "{:5}".format(len(eventdata)) 
                              + ' ' + "{:2}".format(1) 
                              + ' ' + "{:6.1f}".format(sum(eventdata)*60/1000)
                              + ' 1     \n')
                
                tmpstr = ' '
                for j in range(len(eventdata)):
                    tmpstr = tmpstr + "{:7.3f}".format(eventdata[j])
                    if j%10 == 9:
                        output.append(tmpstr + '\n')
                        tmpstr = ' '
                if len(tmpstr) > 5:
                    output.append(tmpstr + '\n')
                eventdata = []
                
                # load the full event
                for j in range(0, nlines):
                    ldata = self.filedata[i+j+1].split()
                    for k in range(len(ldata)):
                        eventdata.append(float(ldata[k]))
                
                output.append(self.filedata[i][:22])
                
            else:
            ## if there is not too long time to the next event
            ## aggregate it with the present event
                
                # add zeroes  
                for j in range(int(diff.total_seconds()/60)):
                    eventdata.append(0.0)
                
                # load the full event
                for j in range(0, nlines):
                    ldata = self.filedata[i+j+1].split()
                    for k in range(len(ldata)):
                        eventdata.append(float(ldata[k]))
                
                
            ## upate end time
            f_end = f_start2 + datetime.timedelta(minutes=eventlen)
            
            i = i + nlines + 1 
        
        output[-1] = (output[-1] + ' ' + "{:5}".format(len(eventdata)) 
                      + ' ' + "{:2}".format(1) 
                      + ' ' + "{:6.1f}".format(sum(eventdata)*60/1000)
                      + ' 1     \n')
        
        tmpstr = ' '
        for j in range(len(eventdata)):
            tmpstr = tmpstr + "{:7.3f}".format(eventdata[j])
            if j%10 == 9:
                output.append(tmpstr + '\n')
                tmpstr = ' '
        output.append(tmpstr + '\n')
        
        if output[-1] == ' \n':
            output = output[:-1]
        
        ## write output
        with open(filenameout, 'w') as o:
            o.writelines(output)
        o.close()
        
        os.startfile(filenameout)


    def create_start_time(self):
        
        ## get starttime from filedata
        meta = self.filedata[0].split()
        start_y = meta[1][0:4]
        start_mo = meta[1][4:6]
        start_d = meta[1][6:8]
        start_h = meta[2][0:2]
        start_mi = meta[2][2:4]
        
        ## set global start time
        self.global_start_time = datetime.datetime(int(start_y), int(start_mo),
                                    int(start_d), int(start_h), int(start_mi))
    

    def create_time_series(self):

        i = 0
        while i < len(self.filedata):
        # get the relevant meta data for the first event
            meta = self.filedata[i].split()
            start_y = meta[1][0:4]
            start_mo = meta[1][4:6]
            start_d = meta[1][6:8]
            start_h = meta[2][0:2]
            start_mi = meta[2][2:4]
            nlines = int(math.ceil(float(meta[4])/10))
            
            ## set the current starttime of the event
            f_start = datetime.datetime(int(start_y), int(start_mo),
                                        int(start_d), int(start_h), int(start_mi))
    
            ## save the starttime of the time series and the current time
            if i == 0:
                step = f_start
                
            while step < f_start:
                self.timeseriesdata.append(0.0)
                step += self.timestep
    
            # load the full event
            for j in range(0, nlines):
                ldata = self.filedata[i+j+1].split()
                for k in range(len(ldata)):
                    self.timeseriesdata.append(float(ldata[k]))
                    step += self.timestep
            
            i = i + j + 2
        
    def getDursDeps(self):
        durs = []
        deps = []
        for i in range(len(self.filedata)):
            if self.filedata[i].startswith('1'):
                durs.append(int(self.filedata[i].split()[4]))
                deps.append(float(self.filedata[i].split()[6]))
                
        return durs, deps
    
    def getMeanMaxIntensities(self, window):
        
        timeseriesdata = self.getRunningMean(window)
        
        meanMaxData = []
        tmp = []
        for i in range(len(timeseriesdata)):
            if timeseriesdata[i] > 0:
                tmp.append(timeseriesdata[i])
            elif len(tmp) > 0:
                meanMaxData.append(max(tmp))
                tmp = []
            else:
                pass
        meanMaxData.append(max(tmp))
        
        return meanMaxData   
    
    def getNoEvents(self):
        j = 0
        for i in range(len(self.filedata)):
            if not self.filedata[i].startswith(' '):
                j = j + 1
                
        return j
    
    def getOriginalData(self):
        return self.filedata
        
    def getRunningMean(self, window):
           
        # data = uniform_filter1d(([0] * math.ceil(window)) + 
        #                         self.getTimeSeriesData() +
        #                         ([0] * math.ceil(window)), size=window)
        
        df = pd.DataFrame(self.getTimeSeriesData(), columns=['data'])
        
        data = df.data.rolling(window).mean()
        data2 = data.to_numpy(na_value=0.0)
        
        ## replace very small values with zeroes
        data2[data2 < 10e-10] = 0.0
        return data2
    
    def getStartTime(self):
        
        ## if start time has not been created do so
        if self.global_start_time == 0:
            self.create_start_time()
        
        ## return start time
        return self.global_start_time
    
    def getTimeSeriesData(self):
        
        ## if data has not been created do it
        if len(self.timeseriesdata) < 1:
            self.create_time_series()
        
        ## return data
        return self.timeseriesdata
    
    def getTimeSeriesLength(self):
            
        ## if data has not been created do it
        if len(self.timeseriesdata) < 1:
            self.create_time_series()
            
        self.lengthInYears = float(len(self.timeseriesdata)) / 60. / 24. / 365.
    
        return self.lengthInYears
    