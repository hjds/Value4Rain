# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:12:31 2021

@author: hjds
"""
# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Engine for retrieving a rain series using the tool
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import sqlite3
import os
# import tempfile
import math
import numpy as np
from Engine.RegionalModel import RegionalModel
from Engine.FileReader import KM2reader
# import pandas as pd

import datetime
# from scipy.ndimage.filters import uniform_filter1d



class RetrieveTimeSeries():
    def __init__(self, args, callback):
        self.args = args
        self.callback = callback
        
        self.callback('\n> Indlæser variable ')
        self.callback
        self.findDurationIndices()
        self.findAvaluesIndicies()
        self.findReturnPeriodIndices()
        self.createParingMask()
        self.findNorthingEasting()
        
        self.setClimateFactors()
        
        self.callback('\n> Indlæser regional model ')
        self.regionalmodel = RegionalModel()
        self.findLocalZValues()

        self.callback('\n> Finder bedst matchende regnserie ')
        self.findBestMatch()
        
        self.callback('\n> Foretager beregninger og skriver outputfil ')
        self.writeOutput()
        

    def calculate_zT_from_observations(self, data, z0, T):

        datalength = self.km2file.getTimeSeriesLength()
        z0 = z0 + 0.000001 ## To secure that rounding errors does not cause extra extremes to be included
        data2 = np.array(data)
        data3 = sorted(data2[data2 > z0] - z0, reverse=True)
    
        n = len(data3)
        n2 = float(n)
        l = n2/(float(datalength))
        m = np.mean(data3)
        b = 0
        for i in range(1,n):
            tmp = (n2-i)/(n2*(n2-1))*(float(data3[i-1])) 
            b = b + tmp
        a = m*(1/((2*b-m)/m)-1)
        k = m/(2*b-m)-2
    
        zT = z0 + a*(1+k)/k * (1- math.pow(1/(l*T),k))
    
        return zT, l, z0, a, k


    def createParingMask(self):
        self.mask = []
        for i in range(len(self.varigheder)):
            for j in range(len(self.gentagelsesperioder)):
                if i >= self.durFrom and i <= self.durTo and j >= self.TFrom and j <= self.TTo:
                    if self.args.metrik.get() == 1:
                        self.mask.append(1)
                    else:
                        self.mask.append(0)
                else:
                    self.mask.append(0)

        for i in range(len(self.avalues)):
            for j in range(len(self.gentagelsesperioder)):
                if j >= 0.5 and j <= 10:
                    if i >= self.aFrom and i <= self.aTo and j >= self.TFrom and j <= self.TTo:
                        if self.args.metrik.get() == 2:
                            self.mask.append(1)
                        else:
                            self.mask.append(0)
                    else:
                        self.mask.append(0)
            
    def findAvaluesIndicies(self):
        with open('data\\afløbstal.txt','r') as f:
            self.avalues = f.readlines()
            
        self.aFrom = [i for i, s in enumerate(self.avalues) if self.args.avaluesFRA.get() == s][0]
        self.aTo = [i for i, s in enumerate(self.avalues) if self.args.avaluesTIL.get() == s][0]

    def findBestMatch(self):
        ## connect to database
        conn = sqlite3.connect('regnserier//regnserier.db')
        c = conn.cursor()
        c.execute('SELECT * FROM regnserier')
        regndata = c.fetchall()
        conn.close()
        
        ## calculate a metric for each time series
        metrics = []
        for r in regndata:
            rMAP = r[4]
            rZs = r[6:]
            
            tmp = abs(float(rMAP)-self.MAP)/self.MAP*sum(self.mask)
            for i in range(len(rZs)):
                tmp = tmp + abs(float(rZs[i])-self.comparisonvalues[i])/self.comparisonvalues[i]*self.mask[i]
            
            metrics.append(tmp)
            
        ## find the time series closest to zero
        bestfitindex = np.argmin(metrics)
        
        self.bestfitseries = regndata[bestfitindex][0]
        self.bestfitvalue = metrics[bestfitindex]
        self.bestfitpath = regndata[bestfitindex][1].strip()
        self.bestfitMAP = regndata[bestfitindex][4]
        self.bestfitdata = regndata[bestfitindex][6:]
        
        regndata2 = []
        for i in range(len(regndata)):
            regndata2.append(regndata[i][1])
        
        self.sortedregndata = [x for _, x in sorted(zip(metrics,regndata2))]
        self.sortedmetrics = sorted(metrics)
        
        conn.close()
    
    def findLocalZValues(self):
        
        ## get MAP
        self.MAP = self.regionalmodel.calculate_MAP_and_CGDmean(self.Northing, self.Easting)[0] * self.CF_MAP
        
        ## get z values
        self.zValues = []
        for a in range(len(self.varigheder)):
            for r in range(len(self.gentagelsesperioder)):
                z = self.regionalmodel.get_zT1(float(self.gentagelsesperioder[r]), self.Northing, self.Easting)
                self.zValues.append(z[a+2] * self.CF[r])
                
        ## get a values
        self.aValuesRegional = []
        for i in range(len(self.avalues)):
            for j in range(len(self.gentagelsesperioder[1:-3])):
                a = self.regionalmodel.get_bassin_vol1(float(self.gentagelsesperioder[j]), self.Northing, self.Easting)
                self.aValuesRegional.append(a[i] * self.CF[j+1])
                
        self.comparisonvalues = self.zValues + self.aValuesRegional
            
    def findDurationIndices(self):
        with open('data\\varigheder.txt','r') as f:
            varigheder = f.readlines()
            self.varigheder = varigheder[2:]
            
        self.durFrom = [i for i, s in enumerate(self.varigheder) if self.args.varighederFRA.get() == s][0]
        self.durTo = [i for i, s in enumerate(self.varigheder) if self.args.varighederTIL.get() == s][0]

    def findNorthingEasting(self):
        ## load DAGI derivative
        with open('data\\kommunekoordinater2.txt', 'r') as f:
            data = f.readlines()
        
        for d in data:
            if self.args.kommune.get().strip() in d:
                self.Northing = float(d.split()[4])
                self.Easting = float(d.split()[3])


    def findReturnPeriodIndices(self):
        with open('data\\gentagelsesperioder.txt','r') as f:
            self.gentagelsesperioder = f.readlines()
            
        self.TFrom = [i for i, s in enumerate(self.gentagelsesperioder) if self.args.gentagelsesperioderFRA.get() == s][0]
        self.TTo = [i for i, s in enumerate(self.gentagelsesperioder) if self.args.gentagelsesperioderTIL.get() == s][0]
        
    def setClimateFactors(self):
        with open('data\\klima_variable.txt','r') as f:
            data = f.readlines()
        
        self.CF = []
        for d in data:
            if self.args.klima.get().strip() in d:
                tmp = d.split('\t')
                self.CF_MAP = float(tmp[1])
                for i in range(2,len(tmp)):
                    self.CF.append(float(tmp[i]))
                
    def writeOutput(self):
        
        ## get variables
                ## get Z0 values and return periods of interest
        with open('data//gentagelsesperioder.txt', 'r') as f:
            data = f.readlines()
        self.return_periods = [float(x) for x in data]
        
        self.z0values = self.regionalmodel.z0values[2:]
        
        with open('data//varigheder.txt', 'r') as f:
            data = f.readlines()
        self.aggregation_periods = [int(x) for x in data]
        self.aggregation_periods = self.aggregation_periods[2:]
        
        ## set up results containers for U and f values
        obszTs = np.zeros([len(self.return_periods),len(self.z0values)])
        # obsLambdas = np.zeros([len(self.return_periods),len(self.z0values)])
        # obsq0s = np.zeros([len(self.return_periods),len(self.z0values)])
        # obsAlphas = np.zeros([len(self.return_periods),len(self.z0values)])
        # obsKappas = np.zeros([len(self.return_periods),len(self.z0values)])
        zTs = np.zeros([len(self.return_periods),len(self.z0values)])
        SzTs = np.zeros([len(self.return_periods),len(self.z0values)])
        # sqrtSzTs = np.zeros([len(self.return_periods),len(self.z0values)])
        # Us = np.zeros([len(self.return_periods),len(self.z0values)])
        # fs = np.zeros([len(self.return_periods),len(self.z0values)])
        obsas = np.zeros([15])      
        regas = np.zeros([15])
        
        # ## load km2 file
        # self.km2file = KM2reader(self.bestfitpath)
        # self.km2file.create_event_max_data()
        # datasets = self.km2file.eventmaxdata
        # self.sorteddatasets = []
        # for d in datasets:
        #     self.sorteddatasets.append(sorted(d, reverse=True))
        
        # ## calculate U and f values relative to the original regional model
        # ## fit local model to the time series
        # for j in range(len(self.return_periods)):
        #     for i in range(len(self.sorteddatasets)):
        #         obszT, obsLambda, obsq0, obsAlpha, obsKappa = self.calculate_zT_from_observations(self.sorteddatasets[i],
        #                                                                                           self.z0values[i],
        #                                                                                           self.return_periods[j])
        #         obszTs[j,i] = obszT  
        #         obsLambdas[j,i] = obsLambda
        #         obsq0s[j,i] = obsq0
        #         obsAlphas[j,i] = obsAlpha
        #         obsKappas[j,i] = obsKappa
                
        ## get values from database
        for j in range(len(self.return_periods)):
            for i in range(len(self.varigheder)):
                obszTs[j,i] = self.bestfitdata[i*(len(self.gentagelsesperioder))+j]
                
        obsas = self.bestfitdata[len(self.return_periods)*len(self.varigheder):]
        
        ## get values from regional model
        for j in range(len(self.return_periods)):

            zT = self.regionalmodel.get_zT1(self.return_periods[j], self.Northing, self.Easting)
            SzT = self.regionalmodel.get_SzT1(self.return_periods[j], self.Northing, self.Easting)
            
            zTs[j] = np.array(zT[2:])
            SzTs[j] = SzT[2:]
            
        for j in range(len(self.return_periods[1:-3])):
            tmp = self.regionalmodel.get_bassin_vol1(self.return_periods[j], self.Northing, self.Easting)
            regas[j] = tmp[0]
            regas[j+5] = tmp[1]
            regas[j+10] = tmp[2]
        
        ## Calculate the actual U and f values
        Us = (obszTs - zTs) / SzTs 
        fs = (zTs - obszTs) / obszTs + 1
        
        fas = (regas - obsas) / obsas + 1


        # print(obszTs)
        # print(zTs)
        
        #find representative values
        fmean = np.mean(fs[3:6,1:5])
        umin = np.min(Us[3:6,1:5])
        umax = np.max(Us[3:6,1:5])
        
        ## create actual output
        nowtime = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        
        oput = nowtime + '\n\n'
        
        oput = oput + ('For ' + str(self.args.kommune.get().strip()) + ' med klimascenarie: "' + str(self.args.klima.get().strip()) + 
                       '"\ner den bedste regnserie: "' + str(self.bestfitpath) + '"\nmed en fitværdi på: ' +
                       str("%.2f" % self.bestfitvalue) + '\nog en årsmiddelnedbør (ÅMN) på: ' + str(int(self.bestfitMAP)) + '\n\n')
        
        oput = oput + 'Klimafaktorer brugt (gentagelseperioder):\nÅMN\t'
        for i in range(len(self.return_periods)):
            oput = oput + str(self.return_periods[i]) + '\t'
        oput = oput + '\n' + str(self.CF_MAP) + '\t'
        for i in range(len(self.CF)):
            oput = oput + str(self.CF[i]) + '\t'
            
        oput = oput + '\n\n'      
        
        oput = oput + ('Der er optimeret på gentagelsesperioder: ' + str(self.gentagelsesperioder[self.TFrom]).strip() + '-' + str(self.gentagelsesperioder[self.TTo]).strip() + ' år.\n')
        
        if self.args.metrik.get() == 1:
            oput = oput + ('Der er optimeret på varigheder: ' + str(self.varigheder[self.durFrom]).strip() + '-' + str(self.varigheder[self.durTo]).strip()  + ' minutter.\n\n')
        else:
            oput = oput + ('Der er optimeret på afløbstal: ' + str(self.avalues[self.aFrom]).strip() + '-' + str(self.avalues[self.aTo]).strip()  + ' 10^6 m/s.\n\n')
        
        oput = oput + ('Den representative værdi af f-værdierne: ' + str("%.2f" % fmean) + '\n' +
                       'U-værdierne variere mellem: ' + str("%.2f" % umin) + ' og ' +
                       str("%.2f" % umax) + '\n\nU værdier\nT\t')
        
        tmpstr = ''
        for i in range(len(self.aggregation_periods)):
            tmpstr = tmpstr + str(self.aggregation_periods[i]) + '\t' 
        oput = oput + tmpstr + '\n'
        tmpstr = ''
        for j in range(len(self.return_periods)):
            tmpstr = tmpstr + str(self.return_periods[j]) + '\t' 
            for i in range(len(self.aggregation_periods)):
                tmpstr = tmpstr + str("%.2f" % Us[j,i]) + '\t'
            tmpstr = tmpstr + '\n'
        oput = oput + tmpstr + '\nf-værdier\nT\t'
        tmpstr = ''
        for i in range(len(self.aggregation_periods)):
            tmpstr = tmpstr + str(self.aggregation_periods[i])+ '\t' 
        oput = oput + tmpstr + '\n'
        tmpstr = ''
        for j in range(len(self.return_periods)):
            tmpstr = tmpstr + str(self.return_periods[j]) + '\t'
            for i in range(len(self.aggregation_periods)):
                tmpstr = tmpstr + str("%.2f" % fs[j,i]) + '\t'
            tmpstr = tmpstr + '\n'
        oput = oput + tmpstr + '\n'
        
        oput = oput +('Representativt fit af boksmodeller (f-værdier)\nT\t')
        for i in range(len(self.avalues)):
            oput = oput + (str(self.avalues[i]).strip() + '\t')
        oput = oput + '\n'
        
        for i in range(len(self.return_periods[1:-3])):
            oput = (oput + str(self.return_periods[i+1]) + '\t' + str("%.2f" % fas[i]) + 
                    '\t' + str("%.2f" % fas[i+5]) + '\t' + str("%.2f" % fas[i+10]) + '\n')
            
        oput = oput + '\n'
        
        oput = oput + ('fitværdierne for de næstbedste serier er:\n\nSerie\tfitværdi\n')
        
        for i in range(1,11):
            oput = oput + (str(self.sortedregndata[i]) + '\t' + str(self.sortedmetrics[i]) + '\n')
        
        
        fulloutputpath = 'rapport_' + str(self.args.kommune.get().strip()) + '_' + nowtime + '.txt'
        with open(fulloutputpath, 'w') as o:
            o.writelines(oput)
            
        os.startfile(fulloutputpath)
        
        