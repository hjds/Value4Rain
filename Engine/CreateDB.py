# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:22:00 2021

@author: hjds
"""
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:12:31 2021

@author: hjds
"""
# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Stand alone script for creating the database behind the tool.
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import sqlite3

## set up or connect to SQlite database
conn = sqlite3.connect('..//regnserier//regnserier.db')
c = conn.cursor()

varigheder = [5,10,30,60,180,360,720,1440,2880,10080]
gentagelsesperioder = [0.1,0.5,1,2,5,10,20,50,100]
avalues = [0.05, 0.1, 1.0]

sqlcall = 'CREATE TABLE IF NOT EXISTS regnserier (ID text, path text, responsible text, method text, MAP float, length float, '
for v in varigheder:
    for g in gentagelsesperioder:
        sqlcall = sqlcall + 'i' + str(v) + 'T' + str(g).replace('.','_') + ' float, '
        
for a in avalues:
    for g in gentagelsesperioder[1:6]:
        sqlcall = sqlcall + 'a' + str(a).replace('.','_') + 'T' + str(g).replace('.','_') + ' float, '

sqlcall = sqlcall + 'PRIMARY KEY (ID))'
## create table
## the ID has to be unique
c.execute(sqlcall)
conn.commit()
conn.close()