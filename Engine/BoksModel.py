# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Boks Model for the Engine for Spildevandskomiteens Regnværktøj
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import numpy as np

class BoksModel():
    def __init__(self, timesseries, a):
        # set afløbstal im mu-m/s/ha
        self.a = a
        # set tidsserie in mu-m/s
        self.timesseries = timesseries
        
        # run the model
        self._runModel()

    def _runModel(self):
        # liste af volumenfyldninger i mm/ha
        self.Y = []

        for i in range(len(self.timesseries)):
            if i > 0:
                y = self.Y[i-1] + self.timesseries[i]*60/1000 - self.a*60/1000
            else:
                y = self.timesseries[i]*60/1000 - self.a*60/1000
                
            if y > 0:
                self.Y.append(y)
            else:
                self.Y.append(0.0)
                
        # liste over maxvolumener skilt af hver gang listen bliver 0
        self.Ymax = []
        # liste over længder på hændelser hvor der er vand i bassin
        self.Ylength = []
        # liste over fyldninger ved start af hændelser
        self.Ystart = []
        
        tmp = []
        for i in range(len(self.Y)):
            if self.Y[i] > 0:
                tmp.append(self.Y[i])
            elif len(tmp) > 0:
                self.Ymax.append(max(tmp))
                self.Ylength.append(len(tmp))
                tmp = []
            else:
                pass
            
        for i in range(1, len(self.timesseries)):
            if self.timesseries[i] > 0 and self.timesseries[i-1] == 0:
                self.Ystart.append(self.Y[i])
                
    ## in mm
    def getSortedBassinVolumes(self):
        # Sorterede volumener (mm/ha)
        self.sortedYmax = sorted(self.Ymax, reverse=True)
        return self.sortedYmax
    
    ## in min
    def getSortedFillingTimes(self):
        # sorterede længder (min)
        self.sortedYlength = sorted(self.Ylength, reverse=True)
        return self.sortedYlength

    ## in mm
    def getSortedStartFillingDegrees(self):
        # sorterede startfyldninger (mm/ha)
        self.sortedYstart = sorted(self.Ystart, reverse=True)
        return self.sortedYstart
    
    ## proportion of the total time where the bassin is wet between 0 and 1
    def getWetDegree(self):
        wetTime = float(np.count_nonzero(self.Y))
        totalTime = float(len(self.Y))
        self.wetDegree = wetTime/totalTime
        return self.wetDegree

