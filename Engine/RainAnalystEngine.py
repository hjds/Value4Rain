# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Engine for Spildevandskomiteens Regnværktøj based on the Engine from
#  DTU Rain Analyst 2014
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import math
import numpy as np
import datetime
from Engine.RegionalModel import RegionalModel
from Engine.FileReader import KM2reader
from Engine.BoksModel import BoksModel
import pandas as pd

class RainAnalystEngine():
    def __init__(self, args, callback):
        self.args = args
        
        ## initiate regional model
        callback('\n> Indlæser regional model ')
        self.regionalmodel = RegionalModel()
        
                ## variables
        with open('data//varigheder.txt', 'r') as f:
            data = f.readlines()
            self.aggregation_periods = [int(x) for x in data]
            self.aggregation_periods = self.aggregation_periods[2:]
            
        with open('data//gentagelsesperioder.txt', 'r') as f:
            data = f.readlines()
            self.return_periods = [float(x) for x in data]
            
        self.z0values = self.regionalmodel.z0values[2:]
        
        
        
        ## get variables associated with input file
        callback('\n> Indlæser regnserie ')
        self.inputfilename = args.regnserie.get().strip()
        self.km2file = KM2reader(self.inputfilename)
        self.sorteddatasets = []
 
        ## get length of data from either input or the file itself
        if args.regnserieLength.get()==1:
            tmp = args.regnserieLEngthFeldt.get().strip()
            if ',' in tmp:
                tmp = tmp.replace(',','.')
            self.regnserielength = float(tmp)
        else:
            self.regnserielength = self.km2file.getTimeSeriesLength()

        self.northing = int(args.northing.get().strip())
        self.easting = int(args.easting.get().strip())


        
        ## get variables associated with output file(s)
        self.outputpath = args.outputSti.get().strip()
        
        ## create list of events
        if args.eventList.get():
            callback('\n> Laver hændelseslister ')
            self.eventListeEndelse = args.eventListeEndelse.get().strip()
            self.create_event_output()
        
        ## create list of ranked events
        if args.rankedList.get():
            callback('\n> Laver regnrækker ')
            self.rankedListEndelse = args.rankedListEndelse.get().strip()
            self.create_ranked_output()
            
        ## create U and f values
        if args.ufValues.get():
            callback('\n> Beregner U- og f-værdier ')
            self.uValuesEndelse = args.uValuesEndelse.get().strip()
            self.fValuesEndelse = args.fValuesEndelse.get().strip()
            self.create_Ufvalues_output()
            

        


    def getRunningMean(self, window):    

        ldata = self.km2file.getTimeSeriesData() + [0]*10080
    
        df = pd.DataFrame(ldata, columns=['data'])
        
        data = df.data.rolling(window).mean()
        data2 = data.to_numpy(na_value=0.0)
        
        ## replace very small values with zeroes
        data2[data2 < 10e-10] = 0.0
        return data2
    
    
    def getRunningMeanLocal(self, data, window):            
    
        df = pd.DataFrame(data, columns=['data'])
        
        data = df.data.rolling(window, min_periods=1).mean()
        data2 = data.to_numpy(na_value=0.0)
        
        ## replace very small values with zeroes
        data2[data2 < 10e-10] = 0.0
        return data2
    
    def getMeanMaxIntensities(self, window):
        
        timeseriesdata = self.getRunningMean(window)
        
        meanMaxData = []
        tmp = []
        for i in range(len(timeseriesdata)):
            if timeseriesdata[i] > 0:
                tmp.append(timeseriesdata[i])
            elif len(tmp) > 0:
                meanMaxData.append(max(tmp))
                tmp = []
            else:
                pass
        
        return meanMaxData
    
    def calculate_zT(self, T, z0, elambda, vlambda, emean, vmean, ekappa, vkappa):
        zT = z0 + emean * (1 + ekappa)/ekappa * (1 - math.pow(1/(elambda*T),ekappa))
        SzT = math.sqrt(math.pow((1+ekappa)*emean*T*math.pow((elambda*T),(-1*ekappa-1)),2)*vlambda+
                        math.pow((1+ekappa)/ekappa*(1-math.pow((1/(T*elambda)),ekappa)),2)*vmean+
                        math.pow((1+ekappa)*emean*(math.pow((elambda*T),(-1*ekappa))/ekappa*
                                                   math.log(elambda*T)-1/math.pow(ekappa,2)*
                                                   (1-math.pow((elambda*T),(-1*ekappa)))),2)*vkappa)
    
        return zT, SzT
    
    def calculate_zT_from_observations(self, data, z0, T):

        datalength = self.regnserielength
        z0 = z0 + 0.000001 ## To secure that rounding errors does not cause extra extremes to be included
        data2 = np.array(data)
        data3 = sorted(data2[data2 > z0] - z0, reverse=True)
    
        n = len(data3)
        n2 = float(n)
        l = n2/(float(datalength))
        m = np.mean(data3)
        b = 0
        for i in range(1,n):
            tmp = (n2-i)/(n2*(n2-1))*(float(data3[i-1])) 
            b = b + tmp
        a = m*(1/((2*b-m)/m)-1)
        k = m/(2*b-m)-2
        
        if T == 0.1:
            try:
                zT = sorted(data2, reverse=True)[int(datalength*10)]
            except:
                zT = sorted(data2, reverse=True)[-1]
        else:
            zT = z0 + a/k * (1 - math.pow(1/(l*T),k)) ## ændret efter input fra Søren / ændret igen efter input fra Henrik
    
        return zT, l, z0, a, k
    
    def create_event_output(self):
    
        ## get dataset from km2-file
        km2data = self.km2file.filedata
        
        # dataset = self.km2file.getTimeSeriesData()
    
        nowtime = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        
        oput = nowtime + '\n\n'
        
        oput = oput + ('Filen "' + str(self.inputfilename) + '" er analyseret for lokationen:\nNorthing: ' +
                       str(self.northing) + '\n' + 'Easting: ' + str(self.easting) + '\n\n')
        
        oput = (oput + 'Total antal hændelser: ' +
                str(self.km2file.getNoEvents()) + '\n \n' + 'YYYY-MM-DD HH:MM:SS\t')
        for a in self.aggregation_periods:
            oput = oput + str(a) + '\t'
        oput = oput + 'Varighed\tDybde\n'
    
        # timestep = datetime.timedelta(0, 60)
  
        i = 0
        while i < (len(km2data)-3):
            meta = km2data[i]
            oput = oput + meta[2:6] + '-' + meta[6:8] + '-' + meta[8:10] + ' ' + meta[11:13] + ':' + meta[13:15] + ':00\t'
            # print(meta[2:6] + '-' + meta[6:8] + '-' + meta[8:10])
            dep = float(meta[32:38])
            dur = int(meta[22:28])
            lines = int(math.ceil(float(dur)/10))
            
            eventdata = [0.0]*10080
            i = i + 1
            for l in range(lines):
                for k in km2data[i+l].split():
                    eventdata.append(float(k))
    
            for j in self.aggregation_periods:
                oput = oput + str("%.3f" % max(self.getRunningMeanLocal(eventdata,j))) + '\t'
    
            oput = oput + str(dur)+ '\t'
            oput = oput + str(dep) + '\n'
    
            i = i + lines
    
            # while dataset[i] == 0:
            #     if i == (len(dataset)-1):
            #         break
            #     i = i+1
    
        fulloutputpath = self.outputpath + '//' + self.inputfilename.split('/')[-1].split('.')[0] + self.eventListeEndelse
        with open(fulloutputpath, 'w') as o:
            o.writelines(oput)

    def create_ranked_output(self): #datasets, infile, outfile, aggregation_periods, datalength, sortdurs, sortdeps):
        
        if len(self.sorteddatasets) < 1:
            ## get the meanMaxInt for each event for each aggregation period and sort them
            self.km2file.create_event_max_data()
            datasets = self.km2file.eventmaxdata
            
            for d in datasets:
                self.sorteddatasets.append(sorted(d, reverse=True))
            # datasets = []
            # for a in range(len(self.aggregation_periods)):
            #     datasets.append(sorted(self.getMeanMaxIntensities(self.aggregation_periods[a]), reverse=True))
        
        durs, deps = self.km2file.getDursDeps()
        
        sortdurs = sorted(durs, reverse=True)
        sortdeps = sorted(deps, reverse=True)
    
        ## create output
        nowtime = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        
        oput = nowtime + '\n\n'
        
        oput = oput + ('Filen "' + str(self.inputfilename) + '" er analyseret for lokationen:\nNorthing: ' +
                       str(self.northing) + '\n' + 'Easting: ' + str(self.easting) + '\n\n')
        
        oput = (oput + 'Total antal hændelser: ' +
                str(self.km2file.getNoEvents()) + '\n \n' + 'Rang\tTcalif\tTmedian\t')
        for a in range(len(self.aggregation_periods)):
            oput = oput + str(self.aggregation_periods[a]) + '\t'
        oput = oput + 'Varighed\tDybde\n'
    
        for i in range(self.km2file.getNoEvents()):
            tmp = float(float(self.regnserielength)/(i+1))
            median = float((float(self.regnserielength) + 0.4)/(i+0.7))
            oput = oput + str(i+1) + '\t' + str("%.2f" % tmp) + '\t' + str("%.2f" % median) + '\t'
            for j in range(len(self.sorteddatasets)):
                if i < len(self.sorteddatasets[j]):
                    oput = oput + str("%.3f" % self.sorteddatasets[j][i]) + '\t'
                else:
                    oput = oput + 'NA\t'
            oput = oput + str(sortdurs[i]) + '\t' + str(sortdeps[i]) + '\n'
        
        fulloutputpath = self.outputpath + '//' + self.inputfilename.split('/')[-1].split('.')[0] + self.rankedListEndelse
        with open(fulloutputpath, 'w') as o:
            o.writelines(oput)

    def create_Ufvalues_output(self):
        
        ## get parameters from the regional model
        # MAP, CGD = self.regionalmodel.calculate_MAP_and_CGDmean(self.northing, self.easting)
        # Elambda, Vlambda, Emean, Vmean, Ekappa, Vkappa = self.regionalmodel.calculate_regional_model_parameters1(self.northing, self.easting)
        
        ## set up results containers
        obszTs = np.zeros([len(self.return_periods),len(self.z0values)])
        obsLambdas = np.zeros([len(self.return_periods),len(self.z0values)])
        obsq0s = np.zeros([len(self.return_periods),len(self.z0values)])
        obsAlphas = np.zeros([len(self.return_periods),len(self.z0values)])
        obsKappas = np.zeros([len(self.return_periods),len(self.z0values)])
        zTs = np.zeros([len(self.return_periods),len(self.z0values)])
        SzTs = np.zeros([len(self.return_periods),len(self.z0values)])
        sqrtSzTs = np.zeros([len(self.return_periods),len(self.z0values)])
        Us = np.zeros([len(self.return_periods),len(self.z0values)])
        fs = np.zeros([len(self.return_periods),len(self.z0values)])
        obsas = np.zeros([15])      
        regas = np.zeros([15])
        
        if len(self.sorteddatasets) < 1:
            ## get the meanMaxInt for each event for each aggregation period and sort them
            self.km2file.create_event_max_data()
            datasets = self.km2file.eventmaxdata
            
            for d in datasets:
                self.sorteddatasets.append(sorted(d, reverse=True))
        # sorteddatasets = []
        # for a in self.aggregation_periods:
        #     sorteddatasets.append(sorted(self.getMeanMaxIntensities(a),reverse=True))
        
        
        ## fit models to the local data
        for j in range(len(self.return_periods)):
            for i in range(len(self.sorteddatasets)):
                obszT, obsLambda, obsq0, obsAlpha, obsKappa = self.calculate_zT_from_observations(self.sorteddatasets[i],
                                                                                                  self.z0values[i],
                                                                                                  self.return_periods[j])
                obszTs[j,i] = obszT
                obsLambdas[j,i] = obsLambda
                obsq0s[j,i] = obsq0
                obsAlphas[j,i] = obsAlpha
                obsKappas[j,i] = obsKappa
                
        self.writeparametersfile(obsLambdas, obsq0s, obsAlphas, obsKappas)

        for j in range(len(self.return_periods)):

            zT = self.regionalmodel.get_zT1(self.return_periods[j], self.northing, self.easting)
            SzT = self.regionalmodel.get_SzT1(self.return_periods[j], self.northing, self.easting)
            
            zTs[j] = zT[2:]
            SzTs[j] = SzT[2:]
        
        ## look at the box mopdel results
        ##run box model on the time series
        ## create box models
        with open('data\\afløbstal.txt','r') as f:
            self.avalues = f.readlines()
        boksmodels = []
        for a in self.avalues:
            boksmodels.append(BoksModel(self.km2file.getTimeSeriesData(), float(a)))
        
        obsas = []        
        for a in range(len(self.avalues)):
            for r in range(len(self.return_periods[1:6])):
                appi = float(self.km2file.getTimeSeriesLength()) / float(self.return_periods[r+1])
                i = math.ceil(appi)
                obsas.append(boksmodels[a].getSortedBassinVolumes()[i])
                
        for j in range(len(self.return_periods[1:-3])):
            tmp = self.regionalmodel.get_bassin_vol1(self.return_periods[j+1], self.northing, self.easting)
            regas[j] = tmp[0]
            regas[j+5] = tmp[1]
            regas[j+10] = tmp[2]
            

        ## Calculate the actual U and f values
        Us = (obszTs - zTs) / SzTs 
        fs = (zTs - obszTs) / obszTs + 1
        
        fas = (regas - obsas) / obsas + 1
      
        #write file to disk
        fmean = np.mean(fs[3:6,1:5])
        umin = np.min(Us[3:6,1:5])
        umax = np.max(Us[3:6,1:5])

        nowtime = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        
        oput = nowtime + '\n\n'
        
        oput = oput + ('Filen "' + str(self.inputfilename) + '" er analyseret for lokationen:\nNorthing: ' +
                       str(self.northing) + '\n' + 'Easting: ' + str(self.easting) + '\n\n')


        oput = oput + ('Den representative værdi af f-værdierne: ' + str("%.2f" % fmean) + '\n' +
                     'U-værdierne variere mellem: ' + str("%.2f" % umin) + ' og ' +
                     str("%.2f" % umax) + '\n\nU værdier\nT\t')
        tmpstr = ''
        for i in range(len(self.aggregation_periods)):
            tmpstr = tmpstr + str(self.aggregation_periods[i]) + '\t' 
        oput = oput + tmpstr + '\n'
        tmpstr = ''
        for j in range(len(self.return_periods)):
            tmpstr = tmpstr + str(self.return_periods[j]) + '\t' 
            for i in range(len(self.aggregation_periods)):
                tmpstr = tmpstr + str("%.2f" % Us[j,i]) + '\t'
            tmpstr = tmpstr + '\n'
        oput = oput + tmpstr + '\nf værdier\nT\t'
        tmpstr = ''
        for i in range(len(self.aggregation_periods)):
            tmpstr = tmpstr + str(self.aggregation_periods[i])+ '\t' 
        oput = oput + tmpstr + '\n'
        tmpstr = ''
        for j in range(len(self.return_periods)):
            tmpstr = tmpstr + str(self.return_periods[j]) + '\t'
            for i in range(len(self.aggregation_periods)):
                tmpstr = tmpstr + str("%.2f" % fs[j,i]) + '\t'
            tmpstr = tmpstr + '\n'
        oput = oput + tmpstr + '\n'          
        
        oput = oput +('Representativt fit af boksmodeller (f-værdier)\nT\t')
        for i in range(len(self.avalues)):
            oput = oput + (str(self.avalues[i]).strip() + '\t')
        oput = oput + '\n'
        
        for i in range(len(self.return_periods[1:-3])):
            oput = (oput + str(self.return_periods[i+1]) + '\t' + str("%.2f" % fas[i]) + 
                    '\t' + str("%.2f" % fas[i+5]) + '\t' + str("%.2f" % fas[i+10]) + '\n')
            
        oput = oput + '\n'
    
        fulloutputpath = self.outputpath + '//' + self.inputfilename.split('/')[-1].split('.')[0] + self.uValuesEndelse
        with open(fulloutputpath, 'w') as o:
            o.writelines(oput)

    def writeparametersfile(self, Lambdas, q0s, Alphas, Kappas):

        nowtime = str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        
        text = nowtime + '\n\n'
        
        text = text + ('Filen "' + str(self.inputfilename) + '" er analyseret for lokationen:\nNorthing: ' +
                       str(self.northing) + '\n' + 'Easting: ' + str(self.easting) + '\n\n')
        
        text = text + 'Parametre i den lokale model:\nLambda: '
        for l in Lambdas[0]:
            text = text + str("%.3f" %l) + ' '
        text = text + '\nq0: '
        for l in q0s[0]:
            text = text + str("%.2f" %l) + ' '
        text = text + '\nAlpha: '
        for l in Alphas[0]:
            text = text + str("%.3f" %l) + ' '
        text = text + '\nKappa: '
        for l in Kappas[0]:
            text = text + str("%.3f" %l) + ' '
            
        fulloutputpath = self.outputpath + '//' + self.inputfilename.split('/')[-1].split('.')[0] + self.fValuesEndelse
        with open(fulloutputpath, 'w') as o:
            o.writelines(text)