# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Regional model for extreme precipitation for the Engine for 
#  Spildevandskomiteens Regnværktøj based on the Engine from
#  DTU Rain Analyst 2014 and the RegionalRegnrække v5.0 tool
#
#  Copyright (C) 2014-2023, Department of Environmental Engineering
#  Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import math

class RegionalModel():
    
    def __init__(self):
        
        self.load_tables()

    ## calculate MAP and CGDmean from northing/easting coordinates
    def calculate_MAP_and_CGDmean(self, northing, easting):

        data = []
        # MAP
        with open('data\\regionalmodel\\MAPdata.txt', 'r') as f:
            data.append(f.readlines()[1:])
            
        # CGDmean
        with open('data\\regionalmodel\\CGDdata.txt', 'r') as f:
            data.append(f.readlines()[1:])
    
        means = []
        ## calculate inverse distance means
        for i in range(len(data)):  
            ## define columns for northing, easting and data
            nc = 0
            ec = 1
            dc = 2
        
            w = []
            m = []
            mean = 0
        
            for d in data[i]:
                ldata = d.split()
                dist = math.sqrt(math.pow(northing - float(ldata[nc]),2)+
                                 math.pow(easting - float(ldata[ec]),2))
                 
                if dist == 0:
                    mean = float(ldata[dc])
                if dist < 15000:
                    weight = 1/(math.pow(dist,2))
                    w.append(weight) 
                    m.append(float(ldata[dc]))
        
            if mean == 0:
                for i in range(len(w)):
                    mean = mean + w[i]/sum(w)*m[i]
                    
            means.append(mean)
    
        return means

    ## shortcut for calculating the regional model parameters fron northing/easting
    def calculate_regional_model_bassin_parameters1(self, northing, easting):
        MAP, CGDmean = self.calculate_MAP_and_CGDmean(northing, easting)
        return self.calculate_regional_model_bassin_parameters2(MAP, CGDmean)

    ## calculate the regional model parameters from MAP and CGDmean
    def calculate_regional_model_bassin_parameters2(self, MAP, CGDmean):
    
        Elambda = []
        Vlambda = []
        Emean = []
        Vmean = []
        Ekappa = []
        Vkappa = []
    
        for ldata in self.lambdabassinparameters:
            d = ldata.split()
            Elambda.append(float(d[1])+float(d[2])*MAP)
            Vlambda.append(float(d[3])+2*float(d[4])*MAP+float(d[5])*math.pow(MAP,2)+float(d[6]))
        for ldata in self.meanbassinparameters:
            d = ldata.split()
            if not d[0] == 'bv0.05':
                Emean.append(float(d[1])+float(d[2])*CGDmean)
                Vmean.append(float(d[3])+2*float(d[4])*CGDmean+float(d[5])*math.pow(CGDmean,2)+float(d[6]))
            else:
                Emean.append(float(d[1])+float(d[2])*MAP)
                Vmean.append(float(d[3])+2*float(d[4])*MAP+float(d[5])*math.pow(MAP,2)+float(d[6]))
        for ldata in self.kappabassinparameters:
            d = ldata.split()
            Ekappa.append(float(d[1]))
            Vkappa.append(float(d[3]))

        return Elambda, Vlambda, Emean, Vmean, Ekappa, Vkappa

    ## shortcut for calculating the regional model parameters fron northing/easting
    def calculate_regional_model_parameters1(self, northing, easting):
        MAP, CGDmean = self.calculate_MAP_and_CGDmean(northing, easting)
        return self.calculate_regional_model_parameters2(MAP, CGDmean)

    ## calculate the regional model parameters from MAP and CGDmean
    def calculate_regional_model_parameters2(self, MAP, CGDmean):
    
        Elambda = []
        Vlambda = []
        Emean = []
        Vmean = []
        Ekappa = []
        Vkappa = []
    
        for ldata in self.lambdaparameters:
            d = ldata.split()
            Elambda.append(float(d[1])+float(d[2])*MAP)
            Vlambda.append(float(d[3])+2*float(d[4])*MAP+float(d[5])*math.pow(MAP,2)+float(d[6]))
        for ldata in self.meanparameters:
            d = ldata.split()
            if not d[0] == 'i7d':
                Emean.append(float(d[1])+float(d[2])*CGDmean)
                Vmean.append(float(d[3])+2*float(d[4])*CGDmean+float(d[5])*math.pow(CGDmean,2)+float(d[6]))
            else:
                Emean.append(float(d[1])+float(d[2])*MAP)
                Vmean.append(float(d[3])+2*float(d[4])*MAP+float(d[5])*math.pow(MAP,2)+float(d[6]))
        for ldata in self.kappaparameters:
            d = ldata.split()
            Ekappa.append(float(d[1]))
            Vkappa.append(float(d[3]))

        return Elambda, Vlambda, Emean, Vmean, Ekappa, Vkappa

    def get_bassin_vol1(self, T, northing, easting):
        MAP, CGDmean = self.calculate_MAP_and_CGDmean(northing, easting)
        return self.get_bassin_vol2(T, MAP, CGDmean)
        
    def get_bassin_vol2(self, T, MAP, CGDmean):
        
        elambda, vlambda, emean, vmean, ekappa, vkappa = self.calculate_regional_model_bassin_parameters2(MAP, CGDmean)
        
        vol = []
        for i in range(len(elambda)):
            tmp = self.bassinz0values[i] + emean[i] * (1 + ekappa[i])/ekappa[i] * (1 - math.pow(1/(elambda[i]*T),ekappa[i]))
            vol.append(tmp)

        return vol
 
    
    ## Get the return values for a given return period, T, and the location expressed in northing/easting
    def get_zT1(self, T, northing, easting):
        MAP, CGDmean = self.calculate_MAP_and_CGDmean(northing, easting)
        return self.get_zT2(T, MAP, CGDmean)

    ## Get the return values for a given return period, T, and MAP + CGDmean
    def get_zT2(self, T, MAP, CGDmean):
        
        elambda, vlambda, emean, vmean, ekappa, vkappa = self.calculate_regional_model_parameters2(MAP, CGDmean)
                       
        zT = []
        for i in range(len(elambda)):
            tmp = self.z0values[i] + emean[i] * (1 + ekappa[i])/ekappa[i] * (1 - math.pow(1/(elambda[i]*T),ekappa[i]))
            zT.append(tmp)
            
        ## weigh the two models for high n values
        if T < 0.333:
            en10, sn10 = self.n10_model(MAP)
            log01 = math.log10(0.1)
            log03 = math.log10(0.33)
            logT = math.log10(T)
            wn10 = 1 - (logT-log01)/(log03-log01)
            wother = 1 - (log03-logT)/(log03-log01)
            
            for i in range(len(zT)):
                zT[i] = wother*zT[i] + wn10*en10[i]

        return zT

    ## Get the uncertainty of the return values for a given return period, T, and the location expressed in northing/easting
    def get_SzT1(self, T, northing, easting):
        MAP, CGDmean = self.calculate_MAP_and_CGDmean(northing, easting)
        result = self.get_SzT2(T, MAP, CGDmean)
        return result
    
    ## Get the uncertainty of the return values for a given return period, T, and MAP + CGDmean
    def get_SzT2(self, T, MAP, CGDmean):
        
        elambda, vlambda, emean, vmean, ekappa, vkappa = self.calculate_regional_model_parameters2(MAP, CGDmean)
        
        SzT = []
        for i in range(len(elambda)):          
            SzT.append(math.sqrt(math.pow((1+ekappa[i])*emean[i]*T*math.pow((elambda[i]*T),(-1*ekappa[i]-1)),2)*vlambda[i]+
                            math.pow((1+ekappa[i])/ekappa[i]*(1-math.pow((1/(T*elambda[i])),ekappa[i])),2)*vmean[i]+
                            math.pow((1+ekappa[i])*emean[i]*(math.pow((elambda[i]*T),(-1*ekappa[i]))/ekappa[i]*
                            math.log(elambda[i]*T)-1/math.pow(ekappa[i],2)*(1-math.pow((elambda[i]*T),(-1*ekappa[i])))),2)*vkappa[i]))    

        ## weigh the two models for high n values        
        if T < 0.333:
            en10, sn10 = self.n10_model(MAP)
            log01 = math.log10(0.1)
            log03 = math.log10(0.33)
            logT = math.log10(T)
            wn10 = 1 - (logT-log01)/(log03-log01)
            wother = 1 - (log03-logT)/(log03-log01)
            
            for i in range(len(SzT)):
                SzT[i] = wother*SzT[i] + wn10*sn10[i]        
        return SzT
           
    ## load in data tables with static values for the regional model from text files
    def load_tables(self):
        
        # LAMBDA
        with open('data\\regionalmodel\\LAMBDA.txt') as f:
            data = f.readlines()
        self.lambdaparameters = data[1:]
        
        ## MEAN
        with open('data\\regionalmodel\\MEAN.txt') as f:
            data = f.readlines()
        self.meanparameters = data[1:]
        
        ## L-CV
        with open('data\\regionalmodel\\L-CV.txt') as f:
            data = f.readlines()
        self.lcvparameters = data[1:]
        
        # KAPPA
        with open('data\\regionalmodel\\KAPPA.txt') as f:
            data = f.readlines()
        self.kappaparameters = data[1:]
        
        # n=10
        with open('data\\regionalmodel\\n10.txt') as f:
            data = f.readlines()
        self.n10parameters = data[1:]
        
        self.z0values = []
        ## set Z0 values
        for l in self.lambdaparameters:
            self.z0values.append(float(l.split()[-1]))
            
        ## basin parameters
        # LAMBDA
        with open('data\\regionalmodel\\LAMBDA-bassin.txt') as f:
            data = f.readlines()
        self.lambdabassinparameters = data[1:]
        
        ## MEAN
        with open('data\\regionalmodel\\MEAN-bassin.txt') as f:
            data = f.readlines()
        self.meanbassinparameters = data[1:]
        
        ## L-CV
        with open('data\\regionalmodel\\L-CV-bassin.txt') as f:
            data = f.readlines()
        self.lcvbassinparameters = data[1:]
        
        # KAPPA
        with open('data\\regionalmodel\\KAPPA-bassin.txt') as f:
            data = f.readlines()
        self.kappabassinparameters = data[1:]        
        
        ## set Z0 values
        self.bassinz0values = []
        for l in self.lambdabassinparameters:
            self.bassinz0values.append(float(l.split()[-1]))
    
    ## the regional model for n=10
    def n10_model(self, MAP):
        
        En10 = []
        Vn10 = []
        
        for ldata in self.n10parameters:
            d = ldata.split()
            En10.append(float(d[1])+float(d[2])*MAP)
            Vn10.append(float(d[3])+2*float(d[4])*MAP+float(d[5])*math.pow(MAP,2)+float(d[6]))
            
        return En10, Vn10