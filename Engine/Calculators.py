# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Engine for handling the calls from the GUI in Spildevandskomiteens Regnværktøj
#
#  Copyright (C) 2014-2023, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

# import sys, time
import traceback
from threading import Thread
from Engine.RainAnalystEngine import RainAnalystEngine
from Engine.AddTimeSeries import AddTimeSeries
from Engine.RetrieveTimeSeries import RetrieveTimeSeries
from Engine.FileReader import KM2reader

class CalcAddTS(Thread):
    def __init__(self, args, callback):
        Thread.__init__(self)
        self.args = args
        self.callback = callback
        
    def run(self):
        try:
            
            ## create engine
            self.callback(" Tjekker alle variable ")
            # engine = RainAnalystEngine(self.args)
            engine = AddTimeSeries(self.args, self.callback)
            
            self.callback('\n> Tilføjer regnserie ')
            engine.addTS()
            
            ## write return message
            msg = "\n> regnserie tilføjet "
            self.callback(msg)
            print('\a')
            
        except:
            e = traceback.format_exc()
            # e = sys.exc_info()
            self.callback(e)
            print('\a')

class CalcSimple(Thread):
    def __init__(self, args, callback):
        Thread.__init__(self)
        self.args = args
        self.callback = callback
        
    def run(self):
        try:
            self.callback(" Undersøger regnserier ")
            ## create engine
            engine = RetrieveTimeSeries(self.args, self.callback)
            
            ## write return message
            self.callback("\n> Regnserie udvalgt ")
            print('\a')
            
        except:
            e = traceback.format_exc()
            # e = sys.exc_info()
            self.callback(e)
            print('\a')
            

class CalcAdvanced(CalcSimple):
    def __init__(self, data, callback):
        CalcSimple.__init__(self, data, callback)
    
    
class CalcRainAnalyst(Thread):
    
    def __init__(self, args, callback):
        Thread.__init__(self)
        self.args = args
        self.callback = callback
        
    def run(self):
        try:
            self.callback(" Starter beregninger ")
            ## create engine
            engine = RainAnalystEngine(self.args, self.callback)
            
            ## write return message
            self.callback("\n> beregningerne er færdige ")
            print('\a')
            
        except:
            e = traceback.format_exc()
            # e = sys.exc_info()
            self.callback(e)
            print('\a')
            
class CalcCreateKM2(Thread):
    
    def __init__(self, args, callback):
        Thread.__init__(self)
        self.args = args
        self.callback = callback
        
    def run(self):
        try:
            self.callback(" Konstruerer ny km2-fil ")
            
            fil = KM2reader(self.args.regnserie.get().strip())
            udfil = self.args.regnserie.get().strip().split('/')[-1][:-4] + '_' + str(self.args.spacing.get().strip()) + '_timer.km2'
            fil.create_alternative_km2_file(int(self.args.spacing.get().strip()), udfil)
            
            self.callback("\n> km2-fil konstrueret ")
            
        except:
            e = traceback.format_exc()
            # e = sys.exc_info()
            self.callback(e)
            print('\a')