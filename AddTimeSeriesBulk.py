# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:12:31 2021

@author: hjds
"""
# -*- coding: utf-8 -*-
#!/usr/bin/env python

#  Engine for adding a rain series to the tool
#
#  Copyright (C) 2014-2022, Department of Environmental and Resource 
#  Engineering, Tehcnical University of Denmark, Hjalte Jomo Danielsen Sørup.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import sqlite3
from Engine.FileReader import KM2reader
import shutil
import random
from Engine.BoksModel import BoksModel
import math
import os

class AddTimeSeries():
    def __init__(self, inputfilename, ID, hvem, metode):

        ## get variables associated with input file
        self.inputfilename = inputfilename
        self.km2file = KM2reader(self.inputfilename)
        
        self.ID = ID
        if '?' in self.ID:
            # set random ID
            self.create_random_ID()
        
        self.hvem = hvem
        self.metode = metode
        
        ## variables
        with open('data//varigheder.txt', 'r') as f:
            data = f.readlines()
            self.aggregation_periods = [int(x) for x in data]
        ## only take periods above 5 minutes
        self.aggregation_periods = self.aggregation_periods[2:]
            
        with open('data//gentagelsesperioder.txt', 'r') as f:
            data = f.readlines()
            self.return_periods = [float(x) for x in data]
            

    def addTS(self):
        if self.km2file.getTimeSeriesLength() > 10:
            self.km2file.calculate_zT_from_observations()
            self.km2file.calculate_MAP()
            self.create_file_path()
            
            ## create box models
            avalues = [0.05, 0.1, 1.0]
            boksmodels = []
            for a in avalues:
                boksmodels.append(BoksModel(self.km2file.getTimeSeriesData(), a))
            
            boksmodelresults = []        
            for a in range(len(avalues)):
                for r in range(len(self.return_periods[1:6])):
                    appi = float(self.km2file.getTimeSeriesLength()) / float(self.return_periods[r+1])
                    i = math.ceil(appi)
                    boksmodelresults.append(boksmodels[a].getSortedBassinVolumes()[i])
    
            sqldata = []
            sqldata.append(str(self.ID))
            sqldata.append(str(self.filepath))
            sqldata.append(str(self.hvem))
            sqldata.append(str(self.metode))
            sqldata.append(float(self.km2file.MAP))
            sqldata.append(float(self.km2file.lengthInYears))
            for a in range(len(self.aggregation_periods)):
                for r in range(len(self.return_periods)):
                    sqldata.append(float(self.km2file.zTs[a][r]))
            for b in boksmodelresults:
                sqldata.append(float(b))
            
            ## convert list to tuble
            
            sqldata2 = [tuple(sqldata)]
            
            sqlstring = 'INSERT OR IGNORE INTO regnserier VALUES (?,?,?,?,?,?'
            for a in self.aggregation_periods:
                    for r in self.return_periods:
                        sqlstring = sqlstring + ',?'
            for b in boksmodelresults:
                sqlstring = sqlstring + ',?'
            sqlstring = sqlstring + ')'
            
            ## connect to database
            conn = sqlite3.connect('regnserier//regnserier.db')
            c = conn.cursor()
            c.executemany(sqlstring, sqldata2)
            conn.commit()
            conn.close()
        
    # def create_file_path(self):
        # ## copy the file to the 'regnserier' folder
        # self.filepath = 'regnserier//' + self.ID + '.km2'
        
        # shutil.copyfile(self.inputfilename, self.filepath)
        
    def create_file_path(self):
        ## copy the file to the 'regnserier' folder
        self.filepath = str(self.ID) + ' - Download fra Spildevandskomiteens dataportal'
        
    
    def create_random_ID(self):
        isnotunique = True
        
        while isnotunique:
            ## create random ID
            self.ID = str(random.randrange(9999,99999))
            
            ## check that it is unique
            conn = sqlite3.connect('regnserier//regnserier.db')
            c = conn.cursor()
            existing = c.execute('SELECT ID FROM regnserier')
            if not self.ID in existing:
                isnotunique = False
                
        conn.commit()
        conn.close()
            
        
os.chdir('C://Users//hjds//GIT-projects//Value4rain')

regnserier = os.listdir('C://Users//hjds//GIT-projects//Value4rain//_obsolete//afg_rettet')
# regnserier = os.listdir('C://Users//hjds//GIT-projects//Value4rain//_obsolete//regnserierDerSkalIndIgen')

for r in regnserier:
    # if r.startswith('9'):
    #     tmp = AddTimeSeries(('C://Users//hjds//GIT-projects//Value4rain//_obsolete//regnserierDerSkalIndIgen//' + r), r[:-4], 'Søren Liedtke Thorndahl, AAU Build', 'AAU Stokastisk')
    # else:
    tmp = AddTimeSeries(('C://Users//hjds//GIT-projects//Value4rain//_obsolete//afg_rettet//' + r), r[:-4], 'Spildevandskomiteen', 'Målt data')
    tmp.addTS()
    print(r + ' added')